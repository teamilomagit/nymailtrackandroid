package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.MailCheckerAdapter;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.util.PhotoPicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MailCheckActivity extends BaseActivity {
    private Button btnSave;
    private Context context;
    private MailCheckerAdapter mAdapter;
    private ArrayList<String> list;
    private String mailboxes = "";
    private CustomProgrssBar loader;
    private PhotoPicker photoPicker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_check);
        init();
    }

    private void init() {
        context = this;

        btnSave = findViewById(R.id.btn_save);
        list = new ArrayList<String>();
        setToolBarWithTitleAndBack(context, "Mail Check");
        loader = new CustomProgrssBar(context);
        photoPicker = new PhotoPicker(context);
        startActivityForResult(new Intent(context, ScannerActivity.class).putExtra("isCameraOpen", false), 199);
        setRycyclerView();
        setOnClick();
    }

    private void setOnClick() {
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (list != null && photoPicker.isPermissionGranted(context)) {
                    mailCheckerAPI();
                }

            }
        });
    }


    private void setRycyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        mAdapter = new MailCheckerAdapter(context, list, new MailCheckerAdapter.MailCheckerAdapterInterface() {
            @Override
            public void getDeleteItemPosition(int position) {
                list.remove(position);

                if (list.size() == 0) {
                    startActivityForResult(new Intent(context, ScannerActivity.class).putExtra("isCameraOpen", false), 199);
                }
                mAdapter.updateAdapter(list);
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 199) {
            if (resultCode == RESULT_OK) {
                list = data.getStringArrayListExtra("mail_check_list");
                mAdapter.updateAdapter(list);
            }
        }
    }


    private void mailCheckerAPI() {
        mailboxes = "";
        for (int i = 0; i < list.size(); i++) {
            mailboxes = list.get(i) + "," + mailboxes;
        }
        mailboxes = mailboxes.substring(0, mailboxes.length() - 1);

        loader.show();
        JSONObject params = new JSONObject();
        try {
            params.put("mailboxes", mailboxes);
            params.put("check_date", new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_MAILCHECKER, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                loader.dismiss();
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, MailCheckActivity.class));
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    /**
     * PERMISSION
     **/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            startActivityForResult(new Intent(context, ScannerActivity.class).putExtra("isCameraOpen", false), 199);
        } else {
            Toast.makeText(context, "Please allow the given permissions", Toast.LENGTH_SHORT).show();
            finish();
        }
    }
}
