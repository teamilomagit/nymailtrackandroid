package com.tiu.nymailtrack.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.util.PhotoPicker;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class ScannerActivity extends BaseActivity implements DecoratedBarcodeView.TorchListener {

    BarcodeReader barcodeReader;
    private Context context;
    TextView txtSkip;
    private PhotoPicker photoPicker;
    String barcodeValue;
    private ArrayList<String> list;


    private CaptureManager capture;
    private DecoratedBarcodeView barcodeScannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanner);
        init();


        //Initialize barcode scanner view
        barcodeScannerView = findViewById(R.id.zxing_barcode_scanner);
        //set torch listener
        barcodeScannerView.setTorchListener(this);

        //start capture
        capture = new CaptureManager(this, barcodeScannerView);
        capture.initializeFromIntent(getIntent(), savedInstanceState);
        capture.decode();
    }

    private void init() {
        context = this;
        list = new ArrayList<String>();

        setToolBarWithTitleAndBack(context, "Scan Tracking Number", new BaseActivityInterface() {
            @Override
            public void ifActivityFinished() {

                if (!getIntent().getBooleanExtra("isShouldFinish", true)) {
                    finish();
                } else {
                    startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    finish();
                }
            }
        });

//        barcodeReader = (BarcodeReader) getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
        txtSkip = findViewById(R.id.txt_skip);
//        photoPicker = new PhotoPicker(context);
        setOnClick();


    }

    private void setOnClick() {

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (getIntent().getBooleanExtra("isCameraOpen", true)) {
                    finish();
                } else {
                    if (list.size() == 0) {
                        Toast.makeText(context, "Please Scan atleast one mailbox barcode!", Toast.LENGTH_SHORT).show();
//                        startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
                    } else {
                        Intent intent = new Intent();
                        intent.putExtra("mail_check_list", list);
                        setResult(RESULT_OK, intent);
                        finish();
                    }
                }
            }
        });
    }

   /* @Override
    public void onScanned(Barcode barcode) {

        // playing barcode reader beep sound
        barcodeReader.playBeep();
        barcodeValue = barcode.displayValue;

        if (getIntent().getBooleanExtra("isCameraOpen", true)) {
            photoPicker.cameraIntent();
        } else {
            if (!isBarcodeScanned(barcodeValue)) {
                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(new Runnable() {

                    @Override
                    public void run() {
                        Toast.makeText(context, "Mailbox ID: " + barcodeValue, Toast.LENGTH_LONG).show();
                    }
                });
                list.add(barcodeValue);
            }
        }

    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {
        *//*for (int i = 0; i < barcodes.size(); i++) {
            if (list.size() < barcodes.size())
                list.add(barcodes.get(i).displayValue);
        }*//*
        Toast.makeText(context, "multi scan", Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {
        Toast.makeText(context, "bitmap scanned", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onScanError(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraPermissionDenied() {
        finish();
    }
*/


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        /*if (data == null) {
            finish();
        }*/

        if (requestCode == PhotoPicker.REQUEST_CAMERA) {
            Uri filePath = photoPicker.onCaptureImageResult();
            if (filePath != null) {
                Intent intent = new Intent();
                intent.putExtra("code", barcodeValue);
                intent.putExtra("image", filePath.toString());
                setResult(Activity.RESULT_OK, intent);
            }
            finish();
        }
    }

    @Override
    public void onBackPressed() {
        /*if (getIntent().getBooleanExtra("isCameraOpen", true)) {
            finish();
        } else {
            if (list.size() == 0) {
                Toast.makeText(context, "Please Scan atleast one mailbox barcode!", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(context, DashboardActivity.class));
            } else {
                finish();
            }
        }*/

        if (!getIntent().getBooleanExtra("isShouldFinish", true)) {
            finish();
        } else {
            startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            finish();
        }
    }


    /**
     * FUNCTIONALITY
     **/
    private boolean isBarcodeScanned(String barcode) {
        for (String code : list) {
            if (code.equalsIgnoreCase(barcode)) {
                return true;
            }
        }

        return false;
    }

    /**ZXING**/
    @Override
    public void onTorchOn() {

    }

    @Override
    public void onTorchOff() {

    }
    @Override
    protected void onResume() {
        super.onResume();
        capture.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        capture.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        capture.onDestroy();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        capture.onSaveInstanceState(outState);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return barcodeScannerView.onKeyDown(keyCode, event) || super.onKeyDown(keyCode, event);
    }
}
