package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;


import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.DashboardAdapter;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.util.AppConstant;

import java.util.ArrayList;

public class DashboardActivity extends BaseActivity {
    Context context;
    RecyclerView recyclerView;
    ArrayList<DashBoardModel> list;
    DashboardAdapter mAdapter;

    private TextView txtBuildMode;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        init();
    }

    private void init() {
        context = this;
        txtBuildMode = findViewById(R.id.txt_build_mode);
        list = new ArrayList<>();
        setData();
        setUpRecyclerview();

        if (AppConstant.API_BASE_URL.contains("dev.")) {
            txtBuildMode.setText("DEV");
        } else {
            txtBuildMode.setText("LIVE");
        }
    }

    private void setData() {
        DashBoardModel model = new DashBoardModel();
        model.setMenu_name("Check In");
        model.setMenu_image(R.mipmap.ic_check_in);
        list.add(model);

        model = new DashBoardModel();
        model.setMenu_name("Check Out");
        model.setMenu_image(R.mipmap.ic_check_out);
        list.add(model);


//        model = new DashBoardModel();
//        model.setMenu_name("Synch");
//        model.setMenu_image(R.mipmap.ic_history);
//        list.add(model);

        model = new DashBoardModel();
        model.setMenu_name("Mail Scan");
        model.setMenu_image(R.mipmap.ic_mail_scan);
        list.add(model);

        model = new DashBoardModel();
        model.setMenu_name("History");
        model.setMenu_image(R.mipmap.ic_history);
        list.add(model);

        model = new DashBoardModel();
        model.setMenu_name("Mail Status");
        model.setMenu_image(R.mipmap.ic_history);
        list.add(model);

        model = new DashBoardModel();
        model.setMenu_name("Logout");
        model.setMenu_image(R.mipmap.ic_logout);
        list.add(model);

//        model = new DashBoardModel();
//        model.setMenu_name("Setting");
//        model.setMenu_image(R.mipmap.ic_setting);
//        list.add(model);

    }

    private void setUpRecyclerview() {
        recyclerView = findViewById(R.id.recyclerview);
        mAdapter = new DashboardAdapter(context, list);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
        recyclerView.setAdapter(mAdapter);
    }

}
