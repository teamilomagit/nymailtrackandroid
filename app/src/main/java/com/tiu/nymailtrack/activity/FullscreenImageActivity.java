package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ImagePagerAdapter;

import java.util.ArrayList;


public class FullscreenImageActivity extends AppCompatActivity {
    private Context context;
    private ImageView imgBack;
    private ImagePagerAdapter mAdapter;
    private ViewPager viewPager;
    private ArrayList<String>list;
    private int position;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_image);

        init();
    }


    private void init() {
        context = this;
        imgBack = findViewById(R.id.img_back);

        setIntentData();
        makeFullScreen();
        setListeners();
        setViewPager();
    }

    private void makeFullScreen() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {

            getWindow().setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                    WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }
    }

    private void setIntentData()
    {
        position = getIntent().getIntExtra("position", 0);
        list = (ArrayList<String>) getIntent().getSerializableExtra("image_list");
    }


    private void setListeners()
    {
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }


    private void setViewPager() {
        viewPager = findViewById(R.id.view_pager);

        mAdapter = new ImagePagerAdapter(context, list);

        viewPager.setAdapter(mAdapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        viewPager.setCurrentItem(position);
    }

}
