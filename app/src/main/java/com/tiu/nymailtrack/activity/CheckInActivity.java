package com.tiu.nymailtrack.activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ImageAdapter;
import com.tiu.nymailtrack.dbHelper.SQLHelper;
import com.tiu.nymailtrack.dialog.ScannerDialog;
import com.tiu.nymailtrack.dialog.SelectionDialog;
import com.tiu.nymailtrack.model.CheckInModel;
import com.tiu.nymailtrack.model.DashBoardModel;

import com.tiu.nymailtrack.service.UploadIntentService;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.AppPreferences;
import com.tiu.nymailtrack.util.FileUploadServiceInterface;
import com.tiu.nymailtrack.util.SingleUploadBroadcastReceiver;
import com.tiu.nymailtrack.util.PhotoPicker;
import com.tiu.nymailtrack.util.SessionManager;
import com.tiu.nymailtrack.util.Utility;
import com.whygraphics.multilineradiogroup.MultiLineRadioGroup;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;


public class CheckInActivity extends BaseActivity implements SingleUploadBroadcastReceiver.Delegate {

    private Context context;
    private AppCompatCheckBox chkEmail, chkSms;
    private Button btnAddImages, btnSave;
    private EditText edtRemark;
    private TextInputEditText edtMailbox;
    private ImageView imgTrackingScanner;
    private RadioButton rdBtnFlat, rdBtn3days, rdBtnPerday, rdBtnBox, rdBtnEnvelope, rdBtnSoftpack;
    private RadioGroup rdGrpAmountType, rdGrpParcel;
    private TextView txtTrackingNumber, txtCourier, txtAmountType, txtAmount;
    private PhotoPicker photoPicker;
    private ImageAdapter imageAdapter;
    private MultiLineRadioGroup mMultiLineRadioGroup;
    private Uri filePath;
    private ArrayList<String> list;
    private String courierID, remindVia, trackingNumber, amount_type = "1", parcel_type = "B";
    private ArrayList<DashBoardModel> dashBoardList;

    //Multipart
    final SingleUploadBroadcastReceiver uploadReceiver = new SingleUploadBroadcastReceiver();
    private BroadcastReceiver trackingNumberReceiver;

    private String jsonStr;
    private boolean isFromEdit = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        init();
    }

    private void init() {
        context = this;

        btnAddImages = findViewById(R.id.btn_add_images);
        btnSave = findViewById(R.id.btn_save);
        chkEmail = findViewById(R.id.chk_email);
        chkSms = findViewById(R.id.chk_sms);
        txtAmount = findViewById(R.id.txt_amount);
        edtRemark = findViewById(R.id.edt_remark);
        rdBtn3days = findViewById(R.id.rd_btn_3days);
        rdBtnBox = findViewById(R.id.rd_btn_box);
        rdBtnEnvelope = findViewById(R.id.rd_btn_envelope);
        rdBtnSoftpack = findViewById(R.id.rd_btn_softpack);
        rdBtnFlat = findViewById(R.id.rd_btn_flat);
        rdBtnPerday = findViewById(R.id.rd_btn_perday);
        rdGrpAmountType = findViewById(R.id.rd_grp_amount_type);
        rdGrpParcel = findViewById(R.id.rd_grp_parcel);
        txtAmountType = findViewById(R.id.txt_amount_type);
        txtCourier = findViewById(R.id.txt_courier);
        imgTrackingScanner = findViewById(R.id.img_tracking_scanner);
        edtMailbox = findViewById(R.id.edt_mailbox);
        txtTrackingNumber = findViewById(R.id.edt_tracking_number);
        mMultiLineRadioGroup = findViewById(R.id.main_activity_multi_line_radio_group);

        list = new ArrayList<>();
        dashBoardList = new ArrayList<>();
        photoPicker = new PhotoPicker(context);
        imageAdapter = new ImageAdapter(context, list);

        setToolBarWithTitleAndBack(context, "Check in", new BaseActivityInterface() {
            @Override
            public void ifActivityFinished() {
              /*  startActivityForResult(new Intent(context, ScannerActivity.class)
                        .putExtra("isCameraOpen", true), 2);*/
                if (jsonStr == null) {
                    if (photoPicker.isPermissionGranted(context)) {
                        setUpScannerDialog();
                    }
                } else {
                    finish();
                }
            }
        });

        // ask permissions for camera and storage
        if (photoPicker.isPermissionGranted(context)) {
            initiateScanner();
        }

       /* Intent uploadIntent = new Intent(context, UploadIntentService.class);
        startService(uploadIntent);*/

//        setUpScannerDialog();

    }

    private void initiateScanner() {
      /*  startActivityForResult(new Intent(context, ScannerActivity.class)
                .putExtra("isCameraOpen", true), 2);*/

        setIntentData();
        setOnClick();
        setGridView();
        setTrackingNumberReceiver();
    }

    private void setTrackingNumberReceiver() {
        IntentFilter intentFilter = new IntentFilter("tracking_number");
        trackingNumberReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                trackingNumber = intent.getStringExtra("barcode");
                showTrackingNumber();
            }
        };

        registerReceiver(trackingNumberReceiver, intentFilter);
    }

    private void setIntentData() {
        jsonStr = getIntent().getStringExtra("json");

        if (jsonStr == null) {
            setUpScannerDialog();
            setData();
        } else {
            try {

                JSONObject jsonObject = new JSONObject(jsonStr);
                edtMailbox.setText(jsonObject.getString("mailboxid"));
                edtRemark.setText(jsonObject.getString("remark"));
                rdGrpAmountType.check(Integer.parseInt(jsonObject.getString("rdGrpAmountType")));
                rdGrpParcel.check(Integer.parseInt(jsonObject.getString("rdGrpParcel")));
                txtCourier.setText(jsonObject.getString("courier_name"));
                mMultiLineRadioGroup.checkAt(Integer.parseInt(jsonObject.getString("mMultiLineRadioGroup")));
                txtAmount.setText(jsonObject.getString("amount"));
                txtTrackingNumber.setText(jsonObject.getString("tracking_number"));
                courierID = jsonObject.getString("courier_type");
                amount_type = jsonObject.getString("amount_type");


                if (jsonObject.getString("photo_list") != null) {
                    String[] images = jsonObject.getString("photo_list").split(",");

                    for (String image : images) {
                        list.add(image);
                    }
                    imageAdapter.updateAdapter(list);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    private void setData() {

        if (AppConstant.constantCheckInModel != null) {
            edtMailbox.setText(AppConstant.constantCheckInModel.getMailboxid());
            edtRemark.setText(AppConstant.constantCheckInModel.getRemark());
            rdGrpAmountType.check(AppConstant.constantCheckInModel.getAmountType());
            rdGrpParcel.check(AppConstant.constantCheckInModel.getParcelType());
            txtCourier.setText(AppConstant.constantCheckInModel.getCourier_type());
            mMultiLineRadioGroup.checkAt(AppConstant.constantCheckInModel.getAmountGroup());
            txtAmount.setText(mMultiLineRadioGroup.getCheckedRadioButtonText().toString().replace(getString(R.string.currency), ""));
            courierID = AppConstant.constantCheckInModel.getCourier_id();
        } else {
            AppConstant.constantCheckInModel = new CheckInModel();
            AppConstant.constantCheckInModel.setAmountType(R.id.rd_btn_flat);
            AppConstant.constantCheckInModel.setParcelType(R.id.rd_btn_box);

            AppConstant.constantCheckInModel.setAmountGroup(mMultiLineRadioGroup.getCheckedRadioButtonIndex());
            AppConstant.constantCheckInModel.setAmount(txtAmount.getText().toString());
            txtAmount.setText(mMultiLineRadioGroup.getCheckedRadioButtonText().toString().replace(getString(R.string.currency), ""));
        }

        Type type = new TypeToken<ArrayList<DashBoardModel>>() {
        }.getType();

        ArrayList<DashBoardModel> courierList = new Gson().fromJson(new AppPreferences(context).getCourierList(), type);
        if (courierList != null) {
            dashBoardList.addAll(courierList);
        }

        radioButton();
    }

    private void resetData() {
        edtMailbox.setText("");
        edtRemark.setText("");
        txtTrackingNumber.setText("");
        list.clear();
        imageAdapter.updateAdapter(list);
    }

    private void updateModel() {
//        AppConstant.constantCheckInModel.setAmount(txtAmount.getText().toString());
        if (AppConstant.constantCheckInModel != null) {
            AppConstant.constantCheckInModel.setMailboxid(edtMailbox.getText().toString());
            AppConstant.constantCheckInModel.setRemark(edtRemark.getText().toString());
        }
    }


    private void checkBox() {

        if (chkSms.isChecked() && chkEmail.isChecked()) {
            remindVia = "EMAIL,SMS";
        } else if (chkEmail.isChecked() && !chkSms.isChecked()) {
            remindVia = "EMAIL";
        } else if (!chkEmail.isChecked() && chkSms.isChecked()) {
            remindVia = "SMS";
        } else
            remindVia = "";

    }

    private void radioButton() {

        if (rdBtnFlat.isChecked())
            txtAmountType.setText("Flat");
        else if (rdBtn3days.isChecked())
            txtAmountType.setText("First 3 days");
        else if (rdBtnPerday.isChecked())
            txtAmountType.setText("Per day");

    }

    private boolean checkValidation() {

        if (edtMailbox.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please Provide Mailbox ID ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (txtCourier.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please Select Courier", Toast.LENGTH_SHORT).show();
            return false;
        } else if (txtAmount.getText().toString().equalsIgnoreCase("")) {
            Toast.makeText(context, "Amount should not be empty ", Toast.LENGTH_SHORT).show();
            return false;
        } /*else if (txtTrackingNumber.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please Scan the Tracking Number ", Toast.LENGTH_SHORT).show();
            return false;
        }*/ else if (!chkSms.isChecked() && !chkEmail.isChecked()) {
            Toast.makeText(context, "Select Checkbox ", Toast.LENGTH_SHORT).show();
            return false;
        } /*else if (list.size() == 0) {
            Toast.makeText(context, "At least one image should be added", Toast.LENGTH_SHORT).show();
            return false;
        }*/

        return true;
    }

    private void setGridView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        recyclerView.setAdapter(imageAdapter);
        recyclerView.setLayoutManager(layoutManager);
    }


    private void setOnClick() {

        btnAddImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photoPicker.isPermissionGranted(context)) {
                    photoPicker.cameraIntent();
                }
            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkValidation()) {
                    checkBox();
//                    uploadImagesUsingMultiPart();
                    setUploadData();
                    /*startActivityForResult(new Intent(context, ScannerActivity.class)
                            .putExtra("isCameraOpen", true)
                            .putExtra("isShouldFinish", true), 2);*/

                    if (jsonStr == null) {
                        setUpScannerDialog();
                    } else {
                        AppConstant.SHOULD_RELOAD = true;
                        finish();
                    }
                }
            }
        });


        imgTrackingScanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*startActivityForResult(new Intent(context, ScannerActivity.class)
                        .putExtra("isShouldFinish", false), 2);*/
                setUpScannerDialog();
            }
        });

        txtCourier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                SelectionDialog selectionDialog = new SelectionDialog(context, dashBoardList, new SelectionDialog.SelectionDialogInterface() {
                    @Override
                    public void getId(String ID, String name) {
                        txtCourier.setText(name);
                        AppConstant.constantCheckInModel.setCourier_type(name);
                        AppConstant.constantCheckInModel.setCourier_id(ID);
                        courierID = ID;
                    }
                });
                selectionDialog.show();

            }
        });

        rdBtnFlat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAmountType.setText("Flat");
                amount_type = "2";
                AppConstant.constantCheckInModel.setAmountType(R.id.rd_btn_flat);
                radioButton();
            }
        });

        rdBtn3days.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAmountType.setText("First 3 days");
                amount_type = "1";
                AppConstant.constantCheckInModel.setAmountType(R.id.rd_btn_3days);
                radioButton();
            }
        });

        rdBtnPerday.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtAmountType.setText("Per day");
                amount_type = "3";
                AppConstant.constantCheckInModel.setAmountType(R.id.rd_btn_perday);
                radioButton();
            }
        });

        rdBtnEnvelope.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parcel_type = "E";
                AppConstant.constantCheckInModel.setParcelType(R.id.rd_btn_envelope);
            }
        });

        rdBtnSoftpack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parcel_type = "S";
                AppConstant.constantCheckInModel.setParcelType(R.id.rd_btn_softpack);
            }
        });

        rdBtnBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parcel_type = "B";
                AppConstant.constantCheckInModel.setParcelType(R.id.rd_btn_box);
            }
        });

        txtAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateModel();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtMailbox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateModel();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        edtRemark.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                updateModel();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtCourier.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                showTrackingNumber();
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        txtTrackingNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (isFromEdit) {
                    trackingNumber = s.toString();
                } else {
                    isFromEdit = true;
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mMultiLineRadioGroup.setOnCheckedChangeListener(new MultiLineRadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(ViewGroup group, RadioButton button) {
                txtAmount.setText(button.getText().toString().replace(getString(R.string.currency), ""));
                AppConstant.constantCheckInModel.setAmountGroup(mMultiLineRadioGroup.getCheckedRadioButtonIndex());
            }
        });

    }

    private String getFormattedAmount(String val) {

        float flVal = Float.parseFloat(val);

        val = String.format("%.2f", flVal);

        return val;
    }

    private void setUpScannerDialog() {
        /*cannerDialog scannerDialog = new ScannerDialog(context, photoPicker);
        scannerDialog.show();*/
        new IntentIntegrator((Activity) context).setCaptureActivity(ScannerActivity.class).initiateScan();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            switch (requestCode) {
                case 1:
                    edtMailbox.setText(data.getStringExtra("code"));
                    break;
                case 2:
                    trackingNumber = data.getStringExtra("code");
                    showTrackingNumber();
                    String path = data.getStringExtra("image");
                    if (path != null) {
                        filePath = Uri.parse(path);
                        list.add(filePath.toString());
                        imageAdapter.updateAdapter(list);
                    }

                    break;
                default:
                    IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    //check for null
                    if (result != null) {
                        if (result.getContents() == null) {
                            Toast.makeText(this, "Scan Cancelled", Toast.LENGTH_LONG).show();
                        } else {
                            //show dialogue with result
                            trackingNumber = result.getContents();
                            photoPicker.cameraIntent();
                        }
                    }
                    showTrackingNumber();
                    break;
            }
        }

        //Profile Picture
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == PhotoPicker.SELECT_FILE) {
                filePath = photoPicker.onSelectFromGalleryResult(data);
                list.add(filePath.toString());
                imageAdapter.updateAdapter(list);

            } else if (requestCode == PhotoPicker.REQUEST_CAMERA) {
                filePath = photoPicker.onCaptureImageResult();
                list.add(PhotoPicker.getPath(filePath, context));
                imageAdapter.updateAdapter(list);
            }
        }

    }

    private void showTrackingNumber() {

        if (trackingNumber != null) {
            if (!trackingNumber.equalsIgnoreCase("")) {
                isFromEdit = false;

                if (txtCourier.getText().toString().contains("FedEx")) {
                    String substring = trackingNumber.substring(Math.max(trackingNumber.length() - 12, 0));
                    txtTrackingNumber.setText(substring);
                } else if (txtCourier.getText().toString().equalsIgnoreCase("USPS")) {
                    String substring = trackingNumber.substring(Math.max(trackingNumber.length() - 22, 0));
                    txtTrackingNumber.setText(substring);
                } else
                    txtTrackingNumber.setText(trackingNumber.toString());
            }
        }

    }

    private void setUploadData() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mailboxid", edtMailbox.getText().toString());
            jsonObject.put("amount", getFormattedAmount(txtAmount.getText().toString()));
            jsonObject.put("courier_type", courierID);
            jsonObject.put("courier_name", txtCourier.getText().toString());
            jsonObject.put("amount_type", amount_type);
            jsonObject.put("parcel_type", parcel_type);
            jsonObject.put("tracking_number", txtTrackingNumber.getText().toString());
            jsonObject.put("remind_via", remindVia);
            jsonObject.put("remark", edtRemark.getText().toString());
            jsonObject.put("receiver_id", new SessionManager(context).getLoginModel().getUser_id());
            jsonObject.put("type", SQLHelper.CHECK_IN_TYPE);


           /* StringBuilder paths = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    paths.append(PhotoPicker.getPath(Uri.parse(list.get(i)), context));
                } else {
                    paths.append(PhotoPicker.getPath(Uri.parse(list.get(i)), context)).append(",");
                }
            }
            jsonObject.put("photo_list", paths.toString());*/

            if (list.size() > 0) {
                StringBuilder paths = new StringBuilder();
                for (int i = 0; i < list.size(); i++) {
                    if (i == list.size() - 1) {
                        paths.append(list.get(i));
                    } else {
                        paths.append(list.get(i)).append(",");
                    }
                }
                jsonObject.put("photo_list", paths.toString());
            }


            if (jsonStr == null) {

                jsonObject.put("scan_id", Utility.getRandomId());
                jsonObject.put("rdGrpAmountType", AppConstant.constantCheckInModel.getAmountType());
                jsonObject.put("rdGrpParcel", AppConstant.constantCheckInModel.getParcelType());
                jsonObject.put("mMultiLineRadioGroup", AppConstant.constantCheckInModel.getAmountGroup());
            } else {
                JSONObject jsonObject1 = new JSONObject(jsonStr);
                jsonObject.put("scan_id", jsonObject1.getString("scan_id"));
                jsonObject.put("rdGrpAmountType", jsonObject1.getString("rdGrpAmountType"));
                jsonObject.put("rdGrpParcel", jsonObject1.getString("rdGrpParcel"));
                jsonObject.put("mMultiLineRadioGroup", jsonObject1.getString("mMultiLineRadioGroup"));
            }


            new SQLHelper(context).updateCheckInData(SQLHelper.CHECK_IN_TYPE, SQLHelper.IN_PROGRESS, jsonObject);
            resetData();
            Intent intent = new Intent(this, UploadIntentService.class);
            intent.putExtra("model", new Gson().toJson(jsonObject));
            startService(intent);

        } catch (JSONException e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }


    }


    private void uploadImagesUsingMultiPart() {

        String uploadId = UUID.randomUUID().toString();

        uploadReceiver.setDelegate(this);
        uploadReceiver.setUploadID(uploadId);


        String path = "";
        if (filePath != null) {


            try {
                MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, uploadId, AppConstant.API_CHCKINS_PACKAGE)
                        .addParameter("mailboxid", edtMailbox.getText().toString())
                        .addParameter("amount", getFormattedAmount(txtAmount.getText().toString()))
                        .addParameter("courier_type", courierID)
                        .addParameter("amount_type", amount_type)
                        .addParameter("parcel_type", parcel_type)
                        .addParameter("tracking_number", txtTrackingNumber.getText().toString())
                        .addParameter("remind_via", remindVia)
                        .addParameter("remark", edtRemark.getText().toString())
                        .addParameter("receiver_id", new SessionManager(context).getLoginModel().getUser_id())
                        .setMaxRetries(2);

                for (int i = 0; i < list.size(); i++) {
                    path = photoPicker.getPath(Uri.parse(list.get(i)), context);
                    multipartUploadRequest.addFileToUpload(path, "package_images[]");
                }

                multipartUploadRequest.startUpload();

                Toast.makeText(context, "Uploading parcel with tracking ID: " + txtTrackingNumber.getText().toString(), Toast.LENGTH_SHORT).show();
                resetData();


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(context, "Error in Uploading", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * MULTIPART HANDLERS
     **/

    @Override
    public void onProgress(int progress) {
//        Toast.makeText(context, "" + progress, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onProgress(long uploadedBytes, long totalBytes) {
    }

    @Override
    public void onError(Exception exception) {
        Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(int serverResponseCode, byte[] serverResponseBody) {
        JSONObject resultObj = null;
        Gson gson = new Gson();

        try {
            String val = new String(serverResponseBody);
            resultObj = new JSONObject(val);

            if (resultObj.getString("status").equalsIgnoreCase("200")) {
                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //show error message
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onCancelled() {

    }

    @Override
    protected void onResume() {
        uploadReceiver.register(context);
        super.onResume();
    }

    @Override
    protected void onPause() {
        uploadReceiver.unregister(context);
        super.onPause();
    }

    @Override
    public void onBackPressed() {
        /*startActivityForResult(new Intent(context, ScannerActivity.class)
                .putExtra("isCameraOpen", true), 2);*/

        setUpScannerDialog();
    }


    /**
     * PERMISSION
     **/

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0
                && grantResults[0] == PackageManager.PERMISSION_GRANTED
                && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
            initiateScanner();
        } else {
            setUpPermissionAlert();
        }
    }

    private void setUpPermissionAlert() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setCancelable(false);
        builder.setTitle("Grant Permission");
        builder.setMessage("This app needs camera and storage permission to scan barcode");
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(context, "Please allow the given permissions", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        builder.setPositiveButton("Grant", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                photoPicker.isPermissionGranted(context);
            }
        });

        builder.show();
    }
}
