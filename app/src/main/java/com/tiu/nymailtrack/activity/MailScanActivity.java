package com.tiu.nymailtrack.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ImageAdapter;
import com.tiu.nymailtrack.dbHelper.SQLHelper;
import com.tiu.nymailtrack.service.UploadIntentService;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.CustomCamera;
import com.tiu.nymailtrack.util.SessionManager;
import com.tiu.nymailtrack.util.SingleUploadBroadcastReceiver;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.util.PhotoPicker;
import com.tiu.nymailtrack.util.Utility;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.UUID;

public class MailScanActivity extends BaseActivity implements SingleUploadBroadcastReceiver.Delegate {

    Context context;
    private Button btnSave, btnAddImages;
    private TextInputEditText edtMailboxID;
    private EditText edtRemark;
    private PhotoPicker photoPicker;
    private Uri filePath;
    private CustomProgrssBar loader;
    private ImageAdapter imageAdapter;
    private ArrayList<String> list;
    private JSONObject jsonObject = new JSONObject();

    private String jsonStr;

    //Multipart
    final SingleUploadBroadcastReceiver uploadReceiver = new SingleUploadBroadcastReceiver();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_scan);

        init();
        setOnClick();
    }


    private void init() {
        context = this;
        setToolBarWithTitleAndBack(context, "Mail Scan");

        btnAddImages = findViewById(R.id.btn_add_images);
        btnSave = findViewById(R.id.btn_save);
        edtMailboxID = findViewById(R.id.edt_mailboxID);
        edtRemark = findViewById(R.id.edt_remark);

        photoPicker = new PhotoPicker(context);
        loader = new CustomProgrssBar(context);
        list = new ArrayList<>();

        setRecyclerView();
        setIntentData();
    }


    private void setIntentData() {
        jsonStr = getIntent().getStringExtra("json");
        if (jsonStr != null) {
            try {
                JSONObject jsonObject = new JSONObject(jsonStr);

                edtMailboxID.setText(jsonObject.getString("mailboxid"));
                edtRemark.setText(jsonObject.getString("remark"));

                String[] images = jsonObject.getString("photo_list").split(",");

                for (String image : images) {
                    list.add(image);
                }

                imageAdapter.updateAdapter(list);

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else {
            if (photoPicker.isPermissionGranted(context)) {
//            photoPicker.cameraIntent();
                startActivityForResult(new Intent(context, CustomCamera.class), 786);
            }
        }
    }

    private void setRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        imageAdapter = new ImageAdapter(context, list);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(context, 3);
        recyclerView.setAdapter(imageAdapter);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setNestedScrollingEnabled(false);
    }

    private void setOnClick() {
        btnAddImages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (photoPicker.isPermissionGranted(context)) {
//                    photoPicker.cameraIntent();
                    if (list.size() > 0) {
                        if (list.size() >= 10) {
                            Toast.makeText(context, "Maximum 10 images allowed", Toast.LENGTH_SHORT).show();
                        } else {
                            startActivityForResult(new Intent(context, CustomCamera.class).putExtra("has_items", list.size()), 786);
                        }
                    } else {
                        startActivityForResult(new Intent(context, CustomCamera.class).putExtra("has_items", 0), 786);
                    }
                }

            }
        });

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkValidation()) {
                    setUploadData();
//                    uploadImagesUsingMultiPart();
                }

            }
        });
    }

    private boolean checkValidation() {
        if (edtMailboxID.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please Provide Mailbox ID ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (list == null) {
            Toast.makeText(context, "Atleast one Image should be added ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Profile Picture
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == PhotoPicker.REQUEST_CAMERA) {
                filePath = photoPicker.onCaptureImageResult();
                list.add(PhotoPicker.getPath(filePath, context));
                imageAdapter.updateAdapter(list);

            } else if (requestCode == 786) {
                ArrayList<String> tempList = data.getStringArrayListExtra("images");
                list.addAll(tempList);
                imageAdapter.updateAdapter(list);
            }
        }

    }

    private void setUploadData() {

        try {
            jsonObject.put("mailboxid", edtMailboxID.getText().toString());
            jsonObject.put("remark", edtRemark.getText().toString());
            jsonObject.put("type", SQLHelper.MAIL_SCAN_TYPE);

            StringBuilder paths = new StringBuilder();
            for (int i = 0; i < list.size(); i++) {
                if (i == list.size() - 1) {
                    paths.append(list.get(i));
                } else {
                    paths.append(list.get(i)).append(",");
                }
            }
            jsonObject.put("photo_list", paths.toString());

            if (jsonStr == null) {
//                int random = (int) (Math.random() * 500 + 1);
                jsonObject.put("scan_id", Utility.getRandomId());
            } else {
                jsonObject.put("scan_id", new JSONObject(jsonStr).getString("scan_id"));
            }

            new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.IN_PROGRESS, jsonObject);
            Intent intent = new Intent(this, UploadIntentService.class);
            intent.putExtra("model", new Gson().toJson(jsonObject));
            startService(intent);
            Toast.makeText(context, "Mail scanning started for " + edtMailboxID.getText().toString(), Toast.LENGTH_SHORT).show();
            finish();

//            uploadImagesUsingMultiPart();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void uploadImagesUsingMultiPart() {

        loader.show();

        String uploadId = UUID.randomUUID().toString();

        uploadReceiver.setDelegate(this);
        uploadReceiver.setUploadID(uploadId);

        String path = "";
        if (list.size() > 0) {

            try {
                MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, uploadId, AppConstant.API_CHECKINS_MAILSCAN)
                        .addParameter("mailboxes", edtMailboxID.getText().toString())
                        .addParameter("remark", edtRemark.getText().toString())
                        .setMaxRetries(2);

                for (int i = 0; i < list.size(); i++) {
//                    path = PhotoPicker.getPath(Uri.parse(list.get(i)), context);
                    path = list.get(i);
                    multipartUploadRequest.addFileToUpload(path, "scan_images[]");
                }

                //  multipartUploadRequest.addHeader("Content-Type", null);
                multipartUploadRequest.startUpload();

            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                loader.dismiss();
            }
        } else {
            loader.dismiss();
            Toast.makeText(context, "Error in Uploading", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    protected void onResume() {
        uploadReceiver.register(context);
        super.onResume();
    }

    @Override
    protected void onPause() {
        uploadReceiver.unregister(context);
        super.onPause();
    }

    @Override
    public void onProgress(int progress) {
    }

    @Override
    public void onProgress(long uploadedBytes, long totalBytes) {
    }

    @Override
    public void onError(Exception exception) {
        loader.dismiss();
        Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(int serverResponseCode, byte[] serverResponseBody) {

        JSONObject resultObj = null;
        Gson gson = new Gson();
        loader.dismiss();

        try {
            resultObj = new JSONObject(new String(serverResponseBody));

            if (resultObj.getString("status").equalsIgnoreCase("200")) {

                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
                new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.SUCCESS, jsonObject);
                finish();
            } else {
                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
//                new SQLHelper(context).updateCheckOutData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.FAIL, jsonObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //show error message
//            new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.FAIL, jsonObject);
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onCancelled() {

    }
}
