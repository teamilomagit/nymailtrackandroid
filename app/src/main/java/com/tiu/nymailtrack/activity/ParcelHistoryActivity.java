package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ParcelHistoryAdapter;
import com.tiu.nymailtrack.model.CheckOutModel;
import com.tiu.nymailtrack.model.ParcelDetailModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.CustomProgrssBar;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ParcelHistoryActivity extends BaseActivity {

    private Context context;
    private Button btnDelivered, btnPending;
    private ParcelHistoryAdapter mAdapter;
    private ArrayList<ParcelDetailModel> list, pendingList, deliveredList;
    private CheckOutModel checkOutModel;
    private CustomProgrssBar loader;
    private View viewDelivered, viewPending;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_history);
        init();
    }

    private void init() {
        context = this;
        btnDelivered = findViewById(R.id.btn_delivered);
        btnPending = findViewById(R.id.btn_pending);
        viewDelivered = findViewById(R.id.view_deliver);
        viewPending = findViewById(R.id.view_pending);
        loader = new CustomProgrssBar(context);


        setToolBarWithTitleAndBack(context, "Parcel History");
        checkOutModel = (CheckOutModel) getIntent().getSerializableExtra("model");
        setRecyclerView();
        setOnClick();
        getParcelDetailAPI();
    }


    private void setRecyclerView() {
        list = new ArrayList<>();
        pendingList = new ArrayList<>();
        deliveredList = new ArrayList<>();

        recyclerView = findViewById(R.id.recycler_view);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        mAdapter = new ParcelHistoryAdapter(context, list);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);
    }

    private void setOnClick() {
        btnDelivered.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewDelivered.setVisibility(View.VISIBLE);
                viewPending.setVisibility(View.INVISIBLE);
                mAdapter.updateAdapter(deliveredList);
                if (deliveredList.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });

        btnPending.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewDelivered.setVisibility(View.INVISIBLE);
                viewPending.setVisibility(View.VISIBLE);
                mAdapter.updateAdapter(pendingList);
                if (pendingList.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                } else {
                    recyclerView.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void getParcelDetailAPI() {

        loader.show();

        JSONObject params = new JSONObject();
        try {
            params.put("account_id", checkOutModel.getId());
            params.put("mailboxes", checkOutModel.getMailboxes());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostJsonArrayAPI(AppConstant.API_PARCELHISTORY, params, ParcelDetailModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                list = (ArrayList<ParcelDetailModel>) resultObject;

                for (ParcelDetailModel temp : list) {
                    if (temp.getStatus().contains("deliver")) {
                        deliveredList.add(temp);
                    } else {
                        pendingList.add(temp);
                    }
                }

                if (pendingList.size() == 0) {
                    recyclerView.setVisibility(View.GONE);
                }

                mAdapter.updateAdapter(pendingList);
                loader.dismiss();
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
