package com.tiu.nymailtrack.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.MailStatusAdapter;
import com.tiu.nymailtrack.dbHelper.SQLHelper;
import com.tiu.nymailtrack.model.MailboxModel;
import com.tiu.nymailtrack.service.UploadIntentService;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.util.PhotoPicker;
import com.tiu.nymailtrack.util.SessionManager;
import com.tiu.nymailtrack.util.SingleUploadBroadcastReceiver;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

public class MailStatusActivity extends BaseActivity implements SingleUploadBroadcastReceiver.Delegate {

    private Context context;
    private Button btnCheckIn, btnCheckOut, btnMailScan;
    private RecyclerView mRecyclerView;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private TextView txtNoData;
    private View viewCheckIn, viewCheckOut, viewMailScan;

    private MailStatusAdapter mAdapter;
    private CustomProgrssBar loader;
    private String selectedType = SQLHelper.CHECK_IN_TYPE;

    //Multipart
    final SingleUploadBroadcastReceiver uploadReceiver = new SingleUploadBroadcastReceiver();
    String DIRECTORY = Environment.getExternalStorageDirectory().toString() + "/NyMail";
    JSONObject jsonObject = new JSONObject();

    //broadcast receiver
    private BroadcastReceiver broadcastReceiverRefresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail_status);

        init();
    }


    private void init() {
        context = this;
        btnCheckIn = findViewById(R.id.btn_check_in);
        btnCheckOut = findViewById(R.id.btn_check_out);
        btnMailScan = findViewById(R.id.btn_mail_scan);
        viewCheckIn = findViewById(R.id.view_check_in);
        viewCheckOut = findViewById(R.id.view_check_out);
        viewMailScan = findViewById(R.id.view_mail_scan);

        txtNoData = findViewById(R.id.txt_no_data);
        loader = new CustomProgrssBar(context);

        setUpRecyclerView();
        setToolBarWithTitleAndBack(context, "MAIL STATUS");
        setListeners();
        setUpRefreshBroadcast();
    }

    private void setListeners() {
        btnCheckIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeHighlightViewsInvisible();

                viewCheckIn.setVisibility(View.VISIBLE);
                getDataFromDb(SQLHelper.CHECK_IN_TYPE);
            }
        });

        btnCheckOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeHighlightViewsInvisible();

                viewCheckOut.setVisibility(View.VISIBLE);
                getDataFromDb(SQLHelper.CHECK_OUT_TYPE);
            }
        });

        btnMailScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeHighlightViewsInvisible();

                viewMailScan.setVisibility(View.VISIBLE);
                getDataFromDb(SQLHelper.MAIL_SCAN_TYPE);
            }
        });
    }

    public void setUpRefreshBroadcast() {
        IntentFilter intentFilter = new IntentFilter("refresh");

        broadcastReceiverRefresh = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                getDataFromDb(selectedType);
            }
        };
        context.registerReceiver(broadcastReceiverRefresh, intentFilter);

    }

    private void makeHighlightViewsInvisible() {
        viewCheckIn.setVisibility(View.INVISIBLE);
        viewCheckOut.setVisibility(View.INVISIBLE);
        viewMailScan.setVisibility(View.INVISIBLE);
    }

    private void setUpRecyclerView() {
        mAdapter = new MailStatusAdapter(context, new ArrayList<JSONObject>());
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);

        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDataFromDb(selectedType);
            }
        });

        getDataFromDb(SQLHelper.CHECK_IN_TYPE);
    }

    private void getDataFromDb(String type) {
        selectedType = type;
        ArrayList<JSONObject> jsonList = new SQLHelper(context).fetchMailStatusWithType(type);

        if (jsonList.size() > 0) {
            txtNoData.setVisibility(View.GONE);
        } else {
            txtNoData.setVisibility(View.VISIBLE);
        }

        mAdapter.updateAdapter(jsonList, new MailStatusAdapter.MailStatusAdapterInterface() {
            @Override
            public void retryUploadForObject(JSONObject json) {
                jsonObject = json;

                Toast.makeText(context, "Uploading data...", Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(context, UploadIntentService.class);
                intent.putExtra("model", new Gson().toJson(jsonObject));
                startService(intent);
                /*if (selectedType.equalsIgnoreCase(SQLHelper.CHECK_IN_TYPE)) {
                    uploadCheckInUsingMultiPart(jsonObject);
                } else if (selectedType.equalsIgnoreCase(SQLHelper.CHECK_OUT_TYPE)) {
                    uploadCheckOutUsingMultiPart(jsonObject);
                } else {
                    uploadMailScanUsingMultiPart(jsonObject);
                }*/
            }
        });
        mSwipeRefreshLayout.setEnabled(false);
    }


    private void uploadCheckInUsingMultiPart(JSONObject jsonObject) {

        String uploadId = UUID.randomUUID().toString();

        uploadReceiver.setDelegate(this);
        uploadReceiver.setUploadID(uploadId);


        String path = "";

        try {
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, uploadId, AppConstant.API_CHCKINS_PACKAGE)
                    .addParameter("mailboxid", jsonObject.getString("mailboxid"))
                    .addParameter("amount", jsonObject.getString("amount"))
                    .addParameter("courier_type", jsonObject.getString("courier_type"))
                    .addParameter("amount_type", jsonObject.getString("amount_type"))
                    .addParameter("parcel_type", jsonObject.getString("parcel_type"))
                    .addParameter("tracking_number", jsonObject.getString("tracking_number"))
                    .addParameter("remind_via", jsonObject.getString("remind_via"))
                    .addParameter("remark", jsonObject.getString("remark"))
                    .addParameter("receiver_id", jsonObject.getString("receiver_id"))
                    .setMaxRetries(2);


            String[] list = jsonObject.getString("photo_list").split(",");

            for (int i = 0; i < list.length; i++) {
                multipartUploadRequest.addFileToUpload(list[i], "package_images[]");
            }

            multipartUploadRequest.startUpload();

            Toast.makeText(context, "Uploading parcel with tracking ID: " + jsonObject.getString("tracking_number"), Toast.LENGTH_SHORT).show();


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    private void uploadCheckOutUsingMultiPart(JSONObject jsonObject) {

        loader.show();

        String uploadId = UUID.randomUUID().toString();

        uploadReceiver.setDelegate(this);
        uploadReceiver.setUploadID(uploadId);

        try {
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, uploadId, AppConstant.API_CHECKOUT_SAVE)
                    .addParameter("account_id", jsonObject.getString("account_id"))
                    .addParameter("mailboxes", jsonObject.getString("mailboxid"))
                    .addParameter("checkinIds", jsonObject.getString("checkinIds"))
                    .addParameter("checkout_type", jsonObject.getString("checkout_type"))
                    .addParameter("receiver_id", jsonObject.getString("receiver_id"))
                    .addParameter("checkout_amount", jsonObject.getString("checkout_amount"))
                    .addParameter("remark", jsonObject.getString("remark"))
                    .setMaxRetries(2);

            String path = jsonObject.getString("signature_image");

            if (path != null) {
                multipartUploadRequest.addFileToUpload(path, "signature_image");
            } else {

                File file = new File(DIRECTORY, "123456" + ".png");


                if (file.exists()) {
                    multipartUploadRequest
                            .addFileToUpload(file.getPath(), "default");
                } else {
                    multipartUploadRequest
                            .addFileToUpload(getURLForResource(), "default");
                }
            }


            multipartUploadRequest.startUpload();


        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            loader.dismiss();
        }

    }

    public String getURLForResource() {

        OutputStream os = null;
        try {
            File file = new File(DIRECTORY, "123456" + ".png");
            os = new FileOutputStream(file);
//            icon.compress(Bitmap.CompressFormat.PNG, 100, os);
            return file.getPath();
        } catch (IOException e) {
            Log.e("combineImages", "problem combining images", e);
        }

        return null;
    }


    private void uploadMailScanUsingMultiPart(JSONObject jsonObject) {

        loader.show();

        String uploadId = UUID.randomUUID().toString();

        uploadReceiver.setDelegate(this);
        uploadReceiver.setUploadID(uploadId);

        String path = "";

        try {
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, uploadId, AppConstant.API_CHECKINS_MAILSCAN)
                    .addParameter("mailboxes", jsonObject.getString("mailboxid"))
                    .addParameter("remark", jsonObject.getString("remark"))
                    .setMaxRetries(2);

            String[] list = jsonObject.getString("photo_list").split(",");

            for (int i = 0; i < list.length; i++) {
                multipartUploadRequest.addFileToUpload(list[i], "package_images[]");
            }

            //  multipartUploadRequest.addHeader("Content-Type", null);
            multipartUploadRequest.startUpload();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            loader.dismiss();
        }

    }


    /**
     * MULTIPART HANDLERS
     **/

    @Override
    public void onProgress(int progress) {

    }

    @Override
    public void onProgress(long uploadedBytes, long totalBytes) {

    }

    @Override
    public void onError(Exception exception) {
        loader.dismiss();
        Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(int serverResponseCode, byte[] serverResponseBody) {
        JSONObject resultObj = null;
        Gson gson = new Gson();
        loader.dismiss();

        try {
            resultObj = new JSONObject(new String(serverResponseBody));

            if (resultObj.getString("status").equalsIgnoreCase("200")) {

                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();

                if (selectedType.equalsIgnoreCase(SQLHelper.CHECK_IN_TYPE)) {
                    new SQLHelper(context).updateCheckInData(selectedType, SQLHelper.SUCCESS, jsonObject);

                } else if (selectedType.equalsIgnoreCase(SQLHelper.CHECK_OUT_TYPE)) {
                    new SQLHelper(context).updateCheckOutData(selectedType, SQLHelper.SUCCESS, jsonObject);

                } else {
                    new SQLHelper(context).updateMailScanData(selectedType, SQLHelper.SUCCESS, jsonObject);
                }

                getDataFromDb(selectedType);

            } else {
                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //show error message
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancelled() {
        loader.dismiss();

    }

    @Override
    protected void onResume() {
        uploadReceiver.register(context);

        if (AppConstant.SHOULD_RELOAD)
        {
            AppConstant.SHOULD_RELOAD = false;
            getDataFromDb(selectedType);
        }

        super.onResume();
    }

    @Override
    protected void onPause() {
        uploadReceiver.unregister(context);
        super.onPause();
    }

}
