package com.tiu.nymailtrack.activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.CheckOutAdapter;
import com.tiu.nymailtrack.model.CheckOutModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.util.PaginationScrollListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class CheckOutActivity extends BaseActivity {

    Context context;
    EditText edtSearch;
    RecyclerView recyclerView;
    CheckOutAdapter checkOutAdapter;
    ArrayList<CheckOutModel> list;
    CustomProgrssBar loader;

    // PAGINATION VARS
    private final int START = 0;
    private final int LIMIT = 20;
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;
    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;
    // indicates the current page which Pagination is fetching.
    private int startIndex = START;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_out);
        init();
    }

    private void init() {
        context = this;
        edtSearch = findViewById(R.id.edt_search);
        recyclerView = findViewById(R.id.recycler_view);
        loader = new CustomProgrssBar(context);

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    resetData();
                    checkoutListAPI(edtSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });

        setLayout();

        checkoutListAPI("");

        setRecyclerView();
    }


    private void setLayout() {
        Boolean flag = getIntent().getBooleanExtra("flag", true);
        if (flag) {
            setToolBarWithTitleAndBack(context, "Check out");
            list = new ArrayList<>();
        } else {
            setToolBarWithTitleAndBack(context, "Customer History");
            list = new ArrayList<>();
        }
    }

    private void setRecyclerView() {
        checkOutAdapter = new CheckOutAdapter(context, list);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setAdapter(checkOutAdapter);
        recyclerView.setLayoutManager(layoutManager);

        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                startIndex = startIndex + LIMIT;

                checkoutListAPI("");
            }

            @Override
            public int getTotalPageCount() {
                return 199;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }

    private void resetData() {
        isLastPage = false;
        startIndex = START;
    }

    /**
     * API CALLING
     **/


    private void checkoutListAPI(String val) {
        loader.show();
        JSONObject params = new JSONObject();
        try {
            params.put("limit", LIMIT);
            params.put("start", startIndex);
            params.put("search", val);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new APIManager().PostJsonArrayAPI(AppConstant.API_CHCKOUT_LIST, params, CheckOutModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                list = (ArrayList<CheckOutModel>) resultObject;

                if (startIndex == START) {
                    checkOutAdapter.clear();
                }

                checkOutAdapter.removeLoadingFooter();
                isLoading = false;

                // if list<10 then last page true
                if (list.size() < LIMIT) {
                    isLastPage = true;
                }

                if (list.size() > 0) {
                    checkOutAdapter.addAll(list);
                }

                // Add loading footer if last page is false
                if (!isLastPage) {
                    checkOutAdapter.addLoadingFooter();
                }

                loader.dismiss();
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (AppConstant.SHOULD_RELOAD) {
            AppConstant.SHOULD_RELOAD = false;
            checkoutListAPI("");
        }
    }
}
