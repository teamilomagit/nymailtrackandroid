package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiu.nymailtrack.BuildConfig;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.AppPreferences;

import org.json.JSONObject;

import java.util.ArrayList;

public class SplashActivity extends AppCompatActivity {
    private Context context;
    private TextView txtVersion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        context = this;
        txtVersion = findViewById(R.id.txt_version);

        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE);


        //set version
        String version_name = BuildConfig.VERSION_NAME;
        int version_code = BuildConfig.VERSION_CODE;
        txtVersion.setText("Version " + version_name + " (" + version_code + ")");

        getCourierData();
    }

    private void getCourierData() {
        JSONObject jsonObject = new JSONObject();
        new APIManager().PostJsonArrayAPI(AppConstant.API_FETCH_COURIER, jsonObject, DashBoardModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                ArrayList<DashBoardModel> list = (ArrayList<DashBoardModel>) resultObject;
                new AppPreferences(context).setCourierList(new Gson().toJson(list));
                pushToLogin();
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                pushToLogin();
            }
        });


    }

    private void pushToLogin() {
        final Thread thread = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(3000);
                    startActivity(new Intent(context, LoginActivity.class));
                    finish();

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();

    }

}
