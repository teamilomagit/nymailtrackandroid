package com.tiu.nymailtrack.activity;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.TextInputEditText;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatCheckBox;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.model.LoginModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.AppPreferences;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.util.SessionManager;
import com.tiu.nymailtrack.util.Utility;

import org.json.JSONException;
import org.json.JSONObject;

public class LoginActivity extends AppCompatActivity {
    private Context context;
    private Button btnLogin;
    private TextInputEditText edtEmail, edtPassword;
    private AppCompatCheckBox chkRemember;
    private CustomProgrssBar progressDialog;
    private AppPreferences appPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();
    }

    private void init() {
        context = this;
        btnLogin = findViewById(R.id.btn_login);
        chkRemember = findViewById(R.id.chk_remember);
        edtEmail = findViewById(R.id.edt_email);
        edtPassword = findViewById(R.id.edt_password);

        progressDialog = new CustomProgrssBar(context);
        appPreferences = new AppPreferences(context);


        if (new SessionManager(context).getLoginModel() != null) {
            startActivity(new Intent(context, DashboardActivity.class));
            finish();
        }

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkValidation()) {
                    loginAPI();
                }
            }
        });


    }

    private boolean checkValidation() {

        if (!Utility.isValidEmail(edtEmail.getText().toString())) {
            Toast.makeText(context, "Please Provide Valid Email Address ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (edtPassword.getText().toString().isEmpty()) {
            Toast.makeText(context, "Please Provide Password ", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }


    private void loginAPI() {
        progressDialog.show();
        JSONObject param = new JSONObject();

        try {
            param.put("identity", edtEmail.getText().toString());
            param.put("password", edtPassword.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_LOGIN, param, LoginModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                progressDialog.dismiss();
                startActivity(new Intent(context, DashboardActivity.class));
                if (chkRemember.isChecked()) {
                    appPreferences.setEmail(edtEmail.getText().toString());
                    appPreferences.setPassword(edtPassword.getText().toString());
                } else {
                    appPreferences.setEmail(null);
                    appPreferences.setPassword(null);
                }
                LoginModel loginModel = (LoginModel) resultObject;
                new SessionManager(context).createLoginSession(loginModel);
                finish();
            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        getUser();
    }

    private void getUser() {

        // appPreferences.setEmail(edtEmail.getText().toString());
        edtEmail.setText(appPreferences.getEmail());
        edtPassword.setText(appPreferences.getPassword());

        if (appPreferences.getEmail() != null && appPreferences.getPassword() != null) {
            chkRemember.setChecked(true);
        }

    }

    @Override
    public void onBackPressed() {
        finishAffinity();
    }
}
