package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ParcelDetailAdapter;
import com.tiu.nymailtrack.model.ParcelDetailModel;
import com.tiu.nymailtrack.util.CustomProgrssBar;

import java.util.ArrayList;

public class ParcelListActivity extends BaseActivity {
    Context context;
    private ParcelDetailAdapter adapter;
    private ArrayList<ParcelDetailModel> list;
    private CustomProgrssBar loader;
    private String selectedIds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_list);

        init();
    }

    private void init() {

        context = this;

        setToolBarWithTitleAndBack(context, getString(R.string.parcel_detail), new BaseActivityInterface() {
            @Override
            public void ifActivityFinished() {
                setDataForResult();
            }
        });

        list = new ArrayList<>();

        list = (ArrayList<ParcelDetailModel>) getIntent().getSerializableExtra("parcel_list");
        selectedIds = getIntent().getStringExtra("selected_ids");


        setRecyclerView();

    }


    private void setRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        loader = new CustomProgrssBar(context);
        adapter = new ParcelDetailAdapter(context, list);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(layoutManager);
        adapter.updateAdapter(list);
    }

    private void setDataForResult()
    {
        Object[] val = adapter.getSelectedItemTotal();

        Intent intent = new Intent();

        intent.putExtra("total", (double)val[0]);
        intent.putExtra("count", (int)val[1]);
        intent.putExtra("selected_ids", (String) val[2]);

        setResult(199, intent);
        finish();
    }


    @Override
    public void onBackPressed() {
        setDataForResult();
    }
}
