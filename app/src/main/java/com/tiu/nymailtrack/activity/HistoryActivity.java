package com.tiu.nymailtrack.activity;


import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.HistoryAdapter;
import com.tiu.nymailtrack.model.CheckOutModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.util.PaginationScrollListener;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HistoryActivity extends BaseActivity {

    private Context context;
    private EditText edtSearch;
    private CustomProgrssBar loader;
    private ArrayList<CheckOutModel> list;
    private HistoryAdapter mAdapter;

    // PAGINATION VARS
    private final int START = 0;
    private final int LIMIT = 20;
    // Indicates if footer ProgressBar is shown (i.e. next page is loading)
    private boolean isLoading = false;
    // If current page is the last page (Pagination will stop after this page load)
    private boolean isLastPage = false;
    // indicates the current page which Pagination is fetching.
    private int startIndex = START;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        init();
    }

    private void init() {
        context = this;
        edtSearch = findViewById(R.id.edt_search);

        list = new ArrayList<>();
        loader = new CustomProgrssBar(context);
        setToolBarWithTitleAndBack(context, "History");
        setRecyclerView();

        loader.show();
        getAppHistoryAPI("");

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    resetData();
                    getAppHistoryAPI(edtSearch.getText().toString());
                    return true;
                }
                return false;
            }
        });
    }

    private void resetData() {
        isLastPage = false;
        startIndex = START;
    }

    private void setRecyclerView() {
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        mAdapter = new HistoryAdapter(context, list);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(mAdapter);

        recyclerView.addOnScrollListener(new PaginationScrollListener(layoutManager) {
            @Override
            protected void loadMoreItems() {
                isLoading = true;
                startIndex = startIndex + LIMIT;
                getAppHistoryAPI("");
            }

            @Override
            public int getTotalPageCount() {
                return 199;
            }

            @Override
            public boolean isLastPage() {
                return isLastPage;
            }

            @Override
            public boolean isLoading() {
                return isLoading;
            }
        });
    }


    private void getAppHistoryAPI(String val) {
        loader.show();
        JSONObject params = new JSONObject();
        try {
            params.put("limit", LIMIT);
            params.put("start", startIndex);
            params.put("search", val);
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }

        new APIManager().PostJsonArrayAPI(AppConstant.API_APPHISTORY, params, CheckOutModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                list = (ArrayList<CheckOutModel>) resultObject;

                if (startIndex == START) {
                    mAdapter.clear();
                }

                mAdapter.removeLoadingFooter();
                isLoading = false;

                // if list<10 then last page true
                if (list.size() < LIMIT) {
                    isLastPage = true;
                }

                if (list.size() > 0) {
                    mAdapter.addAll(list);
                }

                // Add loading footer if last page is false
                if (!isLastPage) {
                    mAdapter.addLoadingFooter();
                }

                loader.dismiss();
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        });

    }
}
