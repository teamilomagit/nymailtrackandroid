package com.tiu.nymailtrack.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputEditText;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ImageAdapter;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.model.PackageCheckoutModel;
import com.tiu.nymailtrack.model.ParcelDetailModel;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.AppPreferences;
import com.tiu.nymailtrack.util.SessionManager;

import java.lang.reflect.Type;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ParcelDetailActivity extends BaseActivity {

    private Context context;
    private CardView cardViewDelivery;
    private ImageView imgSignature;
    private ParcelDetailModel parcelDetailModel;
    private TextInputEditText edtReceivedDate, edtReceivedDateDelivery, edtRemark, edtRemarkDelivery, edtTrackindNumber, edtParcelType, edtMailboxNumber, edtStorageFee, edtCourier, edtCheckoutType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcel_detail);
        init();
    }

    private void init() {
        context = this;
        cardViewDelivery = findViewById(R.id.card_parcel_delivery);
        edtReceivedDate = findViewById(R.id.edt_received_date);
        edtReceivedDateDelivery = findViewById(R.id.edt_received_date_delivery);
        edtCheckoutType = findViewById(R.id.edt_checkout_type);
        edtCourier = findViewById(R.id.edt_courier);
        edtMailboxNumber = findViewById(R.id.edt_mailbox);
        edtParcelType = findViewById(R.id.edt_parcel_type);
        edtRemark = findViewById(R.id.edt_remark);
        edtRemarkDelivery = findViewById(R.id.edt_remark_delivery);
        edtStorageFee = findViewById(R.id.edt_storage_fee);
        edtTrackindNumber = findViewById(R.id.edt_tracking_number);
        imgSignature = findViewById(R.id.img_signature);

        setToolBarWithTitleAndBack(context, "Parcel Details");
        setIntentData();

        imgSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ArrayList<String> temp = new ArrayList<>();
                temp.add(AppConstant.API_BASE_URL + "uploads/" + parcelDetailModel.getCust_account_id() + "/signature_image/" + parcelDetailModel.getPackageCheckout().getSignature_image());

                startActivity(new Intent(context, FullscreenImageActivity.class)
                        .putExtra("position", 0)
                        .putExtra("image_list", temp));
            }
        });
    }

    private void setIntentData() {

        parcelDetailModel = new Gson().fromJson(getIntent().getStringExtra("parcel_model"), ParcelDetailModel.class);

        edtStorageFee.setText(getString(R.string.currency) + " " + parcelDetailModel.getAmount()
                + " (" + getAmountType(parcelDetailModel.getAmount_type()) + ")");

        edtCourier.setText(getCourierName(parcelDetailModel.getCourier_type()));
        edtMailboxNumber.setText(parcelDetailModel.getMailboxlist_number());
        edtTrackindNumber.setText(parcelDetailModel.getTracking_number());
        edtReceivedDate.setText(getFormattedDate(parcelDetailModel.getReceivedate()));
        edtRemarkDelivery.setText(parcelDetailModel.getReceivedate());
        edtParcelType.setText(getParcelType(parcelDetailModel.getParcel_type()));


        if (parcelDetailModel.getRemark().equalsIgnoreCase("")) {
            edtRemark.setText(" - ");
        } else {
            edtRemark.setText(parcelDetailModel.getRemark());
        }

        if (parcelDetailModel.getPackage_images() != null) {
            if (!parcelDetailModel.getPackage_images().equalsIgnoreCase("")) {
                setupRecyclerView();
            }
        }

        if (parcelDetailModel.getPackageCheckout() != null) {
            edtRemarkDelivery.setText(parcelDetailModel.getPackageCheckout().getRemark());
            edtReceivedDateDelivery.setText(getFormattedDate(parcelDetailModel.getPackageCheckout().getCheckoutdate()));
            edtCheckoutType.setText(getCheckoutType(parcelDetailModel.getPackageCheckout().getCheckout_type()));

            if (parcelDetailModel.getPackageCheckout().getRemark().equalsIgnoreCase("")) {
                edtRemarkDelivery.setText(" - ");
            } else {
                edtRemarkDelivery.setText(parcelDetailModel.getRemark());
            }

            if (!parcelDetailModel.getPackageCheckout().getSignature_image().equalsIgnoreCase("")) {
                String url = AppConstant.API_BASE_URL + "uploads/" + parcelDetailModel.getCust_account_id() + "/signature_image/" + parcelDetailModel.getPackageCheckout().getSignature_image();
                Glide.with(context).load(url).thumbnail(0.5f).into(imgSignature);
            }

        } else {
            cardViewDelivery.setVisibility(View.GONE);
        }


    }

    private void setupRecyclerView() {

        ArrayList<String> tempList = new ArrayList<>();

        String[] urls = parcelDetailModel.getPackage_images().split(",");
        for (String url : urls) {
            tempList.add(AppConstant.API_BASE_URL + "uploads/" + parcelDetailModel.getCust_account_id() + "/package_images/" + url);
        }

        ImageAdapter mAdapter = new ImageAdapter(context, tempList);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 3));
        recyclerView.setAdapter(mAdapter);
    }

    /**
     * FUNCTIONALITY
     **/

    private String getFormattedDate(String date) {
        try {
            return new SimpleDateFormat("MM/dd/yyyy")
                    .format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

    private String getCourierName(String courierId) {
        Type type = new TypeToken<ArrayList<DashBoardModel>>() {
        }.getType();

        ArrayList<DashBoardModel> courierList = new Gson().fromJson(new AppPreferences(context).getCourierList(), type);

        for (DashBoardModel dashBoardModel : courierList) {
            if (courierId.equalsIgnoreCase(dashBoardModel.getId())) {
                return dashBoardModel.getName();
            }
        }

        return "";
    }

    private String getCheckoutType(String val) {
        if (val.equalsIgnoreCase("P")) {
            return "Pickup";
        } else {
            return "Forwarded";
        }
    }

    private String getParcelType(String val) {

        if (val.equalsIgnoreCase("B")) {
            return "Box";
        } else if (val.equalsIgnoreCase("E")) {
            return "Envelope";
        } else {
            return "Softpack";
        }
    }

    private String getAmountType(String val) {
        if (val.equalsIgnoreCase("1")) {
            return "First 3 days";
        } else if (val.equalsIgnoreCase("2")) {
            return "Flat";
        } else if (val.equalsIgnoreCase("3")) {
            return "Per day";
        }

        return "";
    }

}
