package com.tiu.nymailtrack.activity;

/**
 * Mohasin
 **/

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.ParcelDetailAdapter;
import com.tiu.nymailtrack.dbHelper.SQLHelper;
import com.tiu.nymailtrack.model.CheckOutModel;
import com.tiu.nymailtrack.model.ParcelDetailModel;
import com.tiu.nymailtrack.service.UploadIntentService;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.PhotoPicker;
import com.tiu.nymailtrack.util.SessionManager;
import com.tiu.nymailtrack.util.SingleUploadBroadcastReceiver;
import com.tiu.nymailtrack.util.CustomProgrssBar;
import com.tiu.nymailtrack.dialog.DigitalSignatureDialog;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.UUID;

public class CustomerDetailActivity extends BaseActivity implements SingleUploadBroadcastReceiver.Delegate {

    private Context context;
    private Button btnCheckout;
    private EditText edtRemark;
    private ImageView imgSignature;
    private RadioButton rdPickup, rdForwarded;
    private RelativeLayout rlParcelCount, rlDueAmount, rlSignature;
    private TextView txtName, txtMobile, txtEmail, txtId, txtCmpName, txtParcelCount, txtDueAmt;
    private CheckOutModel checkOutModel;
    private ArrayList<ParcelDetailModel> list;
    private CustomProgrssBar loader;
    private ParcelDetailAdapter adapter;
    private String selectedIds, checkoutType = "P";
    private Uri filepath;
    private PhotoPicker photoPicker;

    //Multipart
    final SingleUploadBroadcastReceiver uploadReceiver = new SingleUploadBroadcastReceiver();
    String DIRECTORY = Environment.getExternalStorageDirectory().toString() + "/NyMail";
    static String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE};
    JSONObject jsonObject = new JSONObject();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_detail);

        init();
    }

    private void init() {
        context = this;
        setToolBarWithTitleAndBack(context, "Customer Detail");
        checkOutModel = (CheckOutModel) getIntent().getSerializableExtra("model");
        loader = new CustomProgrssBar(context);
        btnCheckout = findViewById(R.id.btn_checkout);
        edtRemark = findViewById(R.id.edt_remark);
        imgSignature = findViewById(R.id.img_sinature);
        rdPickup = findViewById(R.id.rd_pickup);
        rdForwarded = findViewById(R.id.rd_forwarded);
        txtName = findViewById(R.id.txt_name);
        txtMobile = findViewById(R.id.txt_mobile);
        txtCmpName = findViewById(R.id.cmp_name);
        txtEmail = findViewById(R.id.txt_email);
        txtId = findViewById(R.id.txt_id);
        txtParcelCount = findViewById(R.id.txt_parcel_count);
        txtDueAmt = findViewById(R.id.txt_due_amt);
        rlDueAmount = findViewById(R.id.rl_due_amount);
        rlParcelCount = findViewById(R.id.rl_parcel_count);
        rlSignature = findViewById(R.id.rl_signature);
        list = new ArrayList<ParcelDetailModel>();
        photoPicker = new PhotoPicker(context);

        setData();
        setOnClick();

        getParcelDetailAPI();

        //initiate permission
        if (photoPicker.isPermissionGranted(context)) {

        }
    }


    private void setData() {

        if (getIntent() != null) {
            txtName.setText(checkOutModel.getFirst_name() + " " + checkOutModel.getLast_name());
            txtEmail.setText(checkOutModel.getEmail());
            txtId.setText(checkOutModel.getMailboxes());
            txtCmpName.setText(checkOutModel.getCompany());
            txtMobile.setText(checkOutModel.getPhone());
            txtDueAmt.setText("$" + String.format("%.2f", checkOutModel.getPackageTotalDue()));

//            txtParcelCount.setText(checkOutModel.getParcel());
        }
    }


    private void setOnClick() {

        rlParcelCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (list.size() > 0) {
                    startActivityForResult(new Intent(context, ParcelListActivity.class)
                                    .putExtra("account_id", checkOutModel.getId())
                                    .putExtra("mailboxes", checkOutModel.getMailboxes())
                                    .putExtra("selected_ids", selectedIds)
                                    .putExtra("parcel_list", list),
                            199);
                } else {
                    Toast.makeText(context, "No parcels to check out", Toast.LENGTH_SHORT).show();
                }
            }
        });

        rlSignature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (photoPicker.isPermissionGranted(context)) {
                    DigitalSignatureDialog dialog = new DigitalSignatureDialog(context, new DigitalSignatureDialog.SignatureDialogInterface() {
                        @Override
                        public void save(Bitmap bitmap, String path) {
                            imgSignature.setImageBitmap(bitmap);
                            filepath = Uri.parse(path);
                            setUploadData();
                        }
                    });
                    dialog.show();
                }
            }
        });
        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (photoPicker.isPermissionGranted(context)) {
                    if (Integer.parseInt(txtParcelCount.getText().toString()) > 0) {
                        setUploadData();
                    } else {
                        Toast.makeText(context, "Please select at least 1 parcel", Toast.LENGTH_SHORT).show();
                    }
//                    uploadImagesUsingMultiPart();
                }
            }
        });

        rdPickup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkoutType = "P";
            }
        });

        rdForwarded.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkoutType = "F";
            }
        });

    }


    private void saveCheckoutAPI() {

        loader.show();


        JSONObject params = new JSONObject();
        try {
            params.put("account_id", checkOutModel.getId());
            params.put("mailboxes", checkOutModel.getMailboxes());
            params.put("checkinIds", selectedIds);
            params.put("checkout_amount", txtDueAmt.getText());
            params.put("remark", edtRemark.getText());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostAPI(AppConstant.API_CHECKOUT_SAVE, params, null, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                loader.dismiss();
                Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
            }
        });

    }


    private void getParcelDetailAPI() {

        loader.show();

        JSONObject params = new JSONObject();
        try {
            params.put("account_id", checkOutModel.getId());
            params.put("mailboxes", checkOutModel.getMailboxes());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIManager().PostJsonArrayAPI(AppConstant.API_PARCEL_LIST, params, ParcelDetailModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                loader.dismiss();
                list = (ArrayList<ParcelDetailModel>) resultObject;
                ArrayList<ParcelDetailModel> tempList = new ArrayList<>();

                int count = 0;
                double total = 0;

                for (ParcelDetailModel parcelDetailModel : list) {
                    if (!parcelDetailModel.getStatus().equalsIgnoreCase("delivered")) {
                        tempList.add(parcelDetailModel);
                        count++;
                        total = total + parcelDetailModel.getPackageTotalDue();
                    }
                }

                list.clear();
                list.addAll(tempList);

                /*if (list.size() == 0) {
                    // all parcels delivered
                    Toast.makeText(context, "No parcels available", Toast.LENGTH_LONG).show();
                    finish();
                    return;
                }*/

                if (list.size() > 0) {
                    txtDueAmt.setText("$" + String.format("%.2f", total));
                    txtParcelCount.setText("" + count);
                    selectedIds = null;
                    setCheckedValue();
                }
            }

            @Override
            public void onError(String error) {
                loader.dismiss();
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();

            }
        });
    }

    /**
     * Debashish
     **/

    private void setCheckedValue() {

        if (selectedIds == null) {
            selectedIds = "";
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setChecked(true);
                selectedIds = selectedIds + list.get(i).getId() + ",";
            }
            selectedIds = selectedIds.substring(0, selectedIds.length() - 1);
        } else {
            for (int i = 0; i < list.size(); i++) {
                if (selectedIds.contains(list.get(i).getId())) {
                    list.get(i).setChecked(true);
                } else {
                    list.get(i).setChecked(false);
                }
            }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 199) {
            double total = data.getDoubleExtra("total", 0);
            int count = data.getIntExtra("count", 0);

            selectedIds = data.getStringExtra("selected_ids");
            if (selectedIds.length() > 2) {
                selectedIds = selectedIds.substring(0, selectedIds.length() - 1);
            }
            txtParcelCount.setText("" + count);
            txtDueAmt.setText("$" + String.format("%.2f", total));
            setCheckedValue();
        }
    }

    private void setUploadData() {
        try {
            jsonObject.put("account_id", checkOutModel.getId());
            jsonObject.put("mailboxid", checkOutModel.getMailboxes());
            jsonObject.put("checkinIds", selectedIds);
            jsonObject.put("checkout_type", checkoutType);
            jsonObject.put("receiver_id", new SessionManager(context).getLoginModel().getUser_id());
            jsonObject.put("checkout_amount", String.valueOf(txtDueAmt.getText()).substring(1));
            jsonObject.put("remark", edtRemark.getText().toString());
            if (filepath != null) {
                jsonObject.put("signature_image", filepath.toString());
            }
            jsonObject.put("type", SQLHelper.CHECK_OUT_TYPE);

            new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.IN_PROGRESS, jsonObject);

            Intent intent = new Intent(this, UploadIntentService.class);
            intent.putExtra("model", new Gson().toJson(jsonObject));
            startService(intent);
            Toast.makeText(context, "Check out started for " + checkOutModel.getMailboxes(), Toast.LENGTH_SHORT).show();
//            AppConstant.SHOULD_RELOAD = true;
            finish();
//            uploadImagesUsingMultiPart();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void uploadImagesUsingMultiPart() {


        loader.show();

        String uploadId = UUID.randomUUID().toString();

        uploadReceiver.setDelegate(this);
        uploadReceiver.setUploadID(uploadId);

        try {
            MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(context, uploadId, AppConstant.API_CHECKOUT_SAVE)
                    .addParameter("account_id", checkOutModel.getId())
                    .addParameter("mailboxes", checkOutModel.getMailboxes())
                    .addParameter("checkinIds", selectedIds)
                    .addParameter("checkout_type", checkoutType)
                    .addParameter("receiver_id", new SessionManager(context).getLoginModel().getUser_id())
                    .addParameter("checkout_amount", String.valueOf(txtDueAmt.getText()).substring(1))
                    .addParameter("remark", String.valueOf(edtRemark.getText()))
                    .setMaxRetries(2);

            if (filepath != null) {
                multipartUploadRequest.addFileToUpload(String.valueOf(filepath), "signature_image");
            } else {

                File file = new File(DIRECTORY, "123456" + ".png");


                if (file.exists()) {
                    multipartUploadRequest
                            .addFileToUpload(file.getPath(), "default");
                } else {
                    multipartUploadRequest
                            .addFileToUpload(getURLForResource(), "default");
                }
            }


            multipartUploadRequest.startUpload();

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
            loader.dismiss();
        }

    }

    public String getURLForResource() {

        OutputStream os = null;
        try {
            File file = new File(DIRECTORY, "123456" + ".png");
            os = new FileOutputStream(file);
//            icon.compress(Bitmap.CompressFormat.PNG, 100, os);
            return file.getPath();
        } catch (IOException e) {
            Log.e("combineImages", "problem combining images", e);
        }

        return null;
    }

    /**
     * MULTIPART HANDLERS
     **/

    @Override
    public void onProgress(int progress) {

    }

    @Override
    public void onProgress(long uploadedBytes, long totalBytes) {

    }

    @Override
    public void onError(Exception exception) {
        loader.dismiss();
        new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.FAIL, jsonObject);
        Toast.makeText(context, exception.toString(), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCompleted(int serverResponseCode, byte[] serverResponseBody) {
        JSONObject resultObj = null;
        Gson gson = new Gson();
        loader.dismiss();

        try {
            resultObj = new JSONObject(new String(serverResponseBody));

            if (resultObj.getString("status").equalsIgnoreCase("200")) {

                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
                new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.SUCCESS, jsonObject);
                AppConstant.SHOULD_RELOAD = true;
                getParcelDetailAPI();


            } else {
//                new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.FAIL, jsonObject);
                Toast.makeText(context, resultObj.getString("msg"), Toast.LENGTH_SHORT).show();
            }

        } catch (JSONException e) {
            e.printStackTrace();
            //show error message
//            new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.FAIL, jsonObject);
            Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCancelled() {
        loader.dismiss();

    }

    @Override
    protected void onResume() {
        uploadReceiver.register(context);
        super.onResume();
    }

    @Override
    protected void onPause() {
        uploadReceiver.unregister(context);
        super.onPause();
    }
}
