package com.tiu.nymailtrack.activity;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiu.nymailtrack.R;

public class BaseActivity extends AppCompatActivity {
    TextView txtTitle;
    BaseActivityInterface listener;

    public interface BaseActivityInterface {
        void ifActivityFinished();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
    }

    protected void setToolBarWithTitleAndBack(final Context context, String title) {
        txtTitle = findViewById(R.id.txt_toolbar_title);
        txtTitle.setText(title);

        ImageView imgBack = findViewById(R.id.img_back);
        imgBack.setVisibility(View.VISIBLE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((Activity) context).finish();
            }
        });
    }

    protected void setToolBarWithTitleAndBack(final Context context, String title, final BaseActivityInterface listener) {
        txtTitle = findViewById(R.id.txt_toolbar_title);
        txtTitle.setText(title);

        this.listener = listener;

        ImageView imgBack = findViewById(R.id.img_back);
        imgBack.setVisibility(View.VISIBLE);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.ifActivityFinished();
                }
            }
        });
    }
}
