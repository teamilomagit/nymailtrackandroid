package com.tiu.nymailtrack.model;

import java.io.Serializable;

public class ParcelDetailModel implements Serializable {

    private String amount;

    private String mailboxid;

    private String packageTotalDue;

    private String parcel_type;

    private String amount_type;

    private String mailboxlist_number;

    private String receivedate;

    private String cust_account_id;

    private String tracking_number;

    private String days;

    private String id;

    private String courier_type;

    private String status;

    private boolean isChecked;

    private String package_images;

    private String remark;

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    private PackageCheckoutModel packageCheckout;

    public PackageCheckoutModel getPackageCheckout() {
        return packageCheckout;
    }

    public void setPackageCheckout(PackageCheckoutModel packageCheckout) {
        this.packageCheckout = packageCheckout;
    }

    public String getPackage_images() {
        return package_images;
    }

    public void setPackage_images(String package_images) {
        this.package_images = package_images;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMailboxid() {
        return mailboxid;
    }

    public void setMailboxid(String mailboxid) {
        this.mailboxid = mailboxid;
    }

    public double getPackageTotalDue() {
        if (packageTotalDue.equalsIgnoreCase("")) {
            return 0;
        } else {
            return Double.parseDouble(packageTotalDue);
        }
    }

    public void setPackageTotalDue(String packageTotalDue) {
        this.packageTotalDue = packageTotalDue;
    }


    public String getParcel_type() {
        return parcel_type;
    }

    public void setParcel_type(String parcel_type) {
        this.parcel_type = parcel_type;
    }

    public String getAmount_type() {
        return amount_type;
    }

    public void setAmount_type(String amount_type) {
        this.amount_type = amount_type;
    }

    public String getMailboxlist_number() {
        return mailboxlist_number;
    }

    public void setMailboxlist_number(String mailboxlist_number) {
        this.mailboxlist_number = mailboxlist_number;
    }

    public String getReceivedate() {
        return receivedate;
    }

    public void setReceivedate(String receivedate) {
        this.receivedate = receivedate;
    }

    public String getCust_account_id() {
        return cust_account_id;
    }

    public void setCust_account_id(String cust_account_id) {
        this.cust_account_id = cust_account_id;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public String getDays() {
        return days;
    }

    public void setDays(String days) {
        this.days = days;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCourier_type() {
        return courier_type;
    }

    public void setCourier_type(String courier_type) {
        this.courier_type = courier_type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
