package com.tiu.nymailtrack.model;

import java.io.Serializable;

public class PackageCheckoutModel implements Serializable{

    private String checkout_amount;
    private String remark;
    private String signature_image;
    private String checkoutdate;
    private String checkout_type;

    public String getCheckout_amount() {
        return checkout_amount;
    }

    public void setCheckout_amount(String checkout_amount) {
        this.checkout_amount = checkout_amount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getSignature_image() {
        return signature_image;
    }

    public void setSignature_image(String signature_image) {
        this.signature_image = signature_image;
    }

    public String getCheckoutdate() {
        return checkoutdate;
    }

    public void setCheckoutdate(String checkoutdate) {
        this.checkoutdate = checkoutdate;
    }

    public String getCheckout_type() {
        return checkout_type;
    }

    public void setCheckout_type(String checkout_type) {
        this.checkout_type = checkout_type;
    }
}
