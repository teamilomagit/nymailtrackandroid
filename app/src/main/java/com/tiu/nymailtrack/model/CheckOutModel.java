package com.tiu.nymailtrack.model;

import java.io.Serializable;

public class CheckOutModel implements Serializable {

    private String parcel;

    private String phone;

    private String mboxlist;

    private String last_name;

    private String company;

    private String mailboxes;

    private String id;

    private String first_name;

    private String email;

    private String packageTotalDue;

    public String getParcel() {
        return parcel;
    }

    public void setParcel(String parcel) {
        this.parcel = parcel;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMboxlist() {
        return mboxlist;
    }

    public void setMboxlist(String mboxlist) {
        this.mboxlist = mboxlist;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getMailboxes() {
        return mailboxes;
    }

    public void setMailboxes(String mailboxes) {
        this.mailboxes = mailboxes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getPackageTotalDue() {
        return Double.parseDouble(packageTotalDue.replace(",",""));
    }

    public void setPackageTotalDue(String packageTotalDue) {
        this.packageTotalDue = packageTotalDue;
    }

}
