package com.tiu.nymailtrack.model;

public class LoginModel {

    private String identity;

    private String email;

    private String __ci_last_regenerate;

    private String user_id;

    private String old_last_login;

    public String getIdentity ()
    {
        return identity;
    }

    public void setIdentity (String identity)
    {
        this.identity = identity;
    }

    public String getEmail ()
    {
        return email;
    }

    public void setEmail (String email)
    {
        this.email = email;
    }

    public String get__ci_last_regenerate ()
    {
        return __ci_last_regenerate;
    }

    public void set__ci_last_regenerate (String __ci_last_regenerate)
    {
        this.__ci_last_regenerate = __ci_last_regenerate;
    }

    public String getUser_id ()
    {
        return user_id;
    }

    public void setUser_id (String user_id)
    {
        this.user_id = user_id;
    }

    public String getOld_last_login ()
    {
        return old_last_login;
    }

    public void setOld_last_login (String old_last_login)
    {
        this.old_last_login = old_last_login;
    }

}
