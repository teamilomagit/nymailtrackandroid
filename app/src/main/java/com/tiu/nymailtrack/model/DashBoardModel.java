package com.tiu.nymailtrack.model;

public class DashBoardModel {
    String Menu_name;
    int Menu_image;

    String name;
    String id;

    public String getMenu_name() {
        return Menu_name;
    }

    public void setMenu_name(String menu_name) {
        Menu_name = menu_name;
    }

    public int getMenu_image() {
        return Menu_image;
    }

    public void setMenu_image(int menu_image) {
        Menu_image = menu_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
