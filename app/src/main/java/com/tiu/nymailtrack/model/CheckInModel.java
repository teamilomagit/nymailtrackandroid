package com.tiu.nymailtrack.model;

import java.io.Serializable;

public class CheckInModel implements Serializable {

    private String mailboxid;

    private String amount;

    private String courier_type;

    private String tracking_number;

    private String remind_via;

    private String remark;

    private String receiver_id;

    private int amountType;

    private int parcelType;

    private String courier_id;

    public String getCourier_id() {
        return courier_id;
    }

    public void setCourier_id(String courier_id) {
        this.courier_id = courier_id;
    }

    public int getAmountGroup() {
        return amountGroup;
    }

    public void setAmountGroup(int amountGroup) {
        this.amountGroup = amountGroup;
    }

    private int amountGroup;


    public int getParcelType() {
        return parcelType;
    }

    public void setParcelType(int parcelType) {
        this.parcelType = parcelType;
    }

    public int getAmountType() {
        return amountType;
    }

    public void setAmountType(int amountType) {
        this.amountType = amountType;
    }

    public String getMailboxid() {
        return mailboxid;
    }

    public void setMailboxid(String mailboxid) {
        this.mailboxid = mailboxid;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCourier_type() {
        return courier_type;
    }

    public void setCourier_type(String courier_type) {
        this.courier_type = courier_type;
    }

    public String getTracking_number() {
        return tracking_number;
    }

    public void setTracking_number(String tracking_number) {
        this.tracking_number = tracking_number;
    }

    public String getRemind_via() {
        return remind_via;
    }

    public void setRemind_via(String remind_via) {
        this.remind_via = remind_via;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getReceiver_id() {
        return receiver_id;
    }

    public void setReceiver_id(String receiver_id) {
        this.receiver_id = receiver_id;
    }
}
