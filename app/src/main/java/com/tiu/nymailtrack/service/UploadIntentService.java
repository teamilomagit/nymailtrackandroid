package com.tiu.nymailtrack.service;

import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.error.AuthFailureError;
import com.android.volley.error.VolleyError;
import com.android.volley.request.SimpleMultiPartRequest;
import com.android.volley.request.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.dbHelper.SQLHelper;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.SessionManager;
import com.tiu.nymailtrack.util.SingleUploadBroadcastReceiver;

import net.gotev.uploadservice.MultipartUploadRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.http.Body;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

import static com.android.volley.VolleyLog.TAG;

public class UploadIntentService extends IntentService {
    private Context context;
    Bitmap bitmap = null;

    //Multipart
    final SingleUploadBroadcastReceiver uploadReceiver = new SingleUploadBroadcastReceiver();


    public UploadIntentService() {
        super("UploadIntentService");
        context = this;
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        uploadReceiver.register(context);


        JSONObject jsonObject = new Gson().fromJson(intent.getStringExtra("model"), JSONObject.class);

        String type = "";

        try {
            type = jsonObject.getString("type");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //check type and upload accordingly
        if (type.equalsIgnoreCase(SQLHelper.CHECK_IN_TYPE)) {
            uploadCheckInMultiFile(jsonObject);
        } else if (type.equalsIgnoreCase(SQLHelper.CHECK_OUT_TYPE)) {
            uploadCheckOutMultiFile(jsonObject);
        } else if (type.equalsIgnoreCase(SQLHelper.MAIL_SCAN_TYPE)) {
            uploadMailScanMultiFile(jsonObject);
        }
    }


    public interface WebServicesCheckInAPI {
        @POST("checkins/package")
        Call<ResponseBody> uploadMultiFile(@Body RequestBody file);
    }

    public interface WebServicesCheckOutAPI {
        @POST("Checkouts/checkoutSave")
        Call<ResponseBody> uploadMultiFile(@Body RequestBody file);
    }

    public interface WebServicesMailScanAPI {
        @POST("custmailboxes/mailscanner")
        Call<ResponseBody> uploadMultiFile(@Body RequestBody file);
    }


    private void uploadCheckInMultiFile(final JSONObject jsonObject) {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        try {
            builder.addFormDataPart("mailboxid", jsonObject.getString("mailboxid"));
            builder.addFormDataPart("amount", jsonObject.getString("amount"));
            builder.addFormDataPart("courier_type", jsonObject.getString("courier_type"));
            builder.addFormDataPart("amount_type", jsonObject.getString("amount_type"));
            builder.addFormDataPart("parcel_type", jsonObject.getString("parcel_type"));
            builder.addFormDataPart("tracking_number", jsonObject.getString("tracking_number"));
            builder.addFormDataPart("remind_via", jsonObject.getString("remind_via"));
            builder.addFormDataPart("remark", jsonObject.getString("remark"));
            builder.addFormDataPart("receiver_id", jsonObject.getString("receiver_id"));

            // Map is used to multipart the file using okhttp3.RequestBody
            // Multiple Images
            try {
                String[] filePaths = jsonObject.getString("photo_list").split(",");

                for (int i = 0; i < filePaths.length; i++) {
                    File file = new File(filePaths[i]);
                    builder.addFormDataPart("package_images[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstant.API_BASE_URL)
                    .build();


            WebServicesCheckInAPI uploadService = retrofit.create(WebServicesCheckInAPI.class);

            MultipartBody requestBody = builder.build();
            Call<ResponseBody> call = uploadService.uploadMultiFile(requestBody);
            call.enqueue(new Callback<ResponseBody>() {


                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {

                        JSONObject json = new JSONObject(response.body().string());
                        if (json.getString("status").equalsIgnoreCase("200")) {
                            if (json.getString("msg").equalsIgnoreCase(AppConstant.ALREADY_EXIST_MSG)) {
                                new SQLHelper(context).updateCheckInData(SQLHelper.CHECK_IN_TYPE, SQLHelper.ALREADY_EXIST, jsonObject);
                            } else {
                                new SQLHelper(context).updateCheckInData(SQLHelper.CHECK_IN_TYPE, SQLHelper.SUCCESS, jsonObject);
                            }
                        } else {
                            if (json.getString("msg").equalsIgnoreCase(AppConstant.MAIL_BOX_DOES_NOT_EXIST)) {
                                new SQLHelper(context).updateCheckInData(SQLHelper.CHECK_IN_TYPE, SQLHelper.MAIL_ID_DOES_NOT_EXIST, jsonObject);
                            } else {
                                new SQLHelper(context).updateCheckInData(SQLHelper.CHECK_IN_TYPE, SQLHelper.FAIL, jsonObject);
                            }
                        }

                        Toast.makeText(context, json.getString("msg"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Toast.makeText(UploadIntentService.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    new SQLHelper(context).updateCheckInData(SQLHelper.CHECK_IN_TYPE, SQLHelper.FAIL, jsonObject);
                    Log.d(TAG, "Error " + t.getMessage());
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void uploadCheckOutMultiFile(final JSONObject jsonObject) {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        try {
            builder.addFormDataPart("account_id", jsonObject.getString("account_id"));
            builder.addFormDataPart("mailboxes", jsonObject.getString("mailboxid"));
            builder.addFormDataPart("checkinIds", jsonObject.getString("checkinIds"));
            builder.addFormDataPart("checkout_type", jsonObject.getString("checkout_type"));
            builder.addFormDataPart("receiver_id", jsonObject.getString("receiver_id"));
            builder.addFormDataPart("checkout_amount", jsonObject.getString("checkout_amount"));
            builder.addFormDataPart("remark", jsonObject.getString("remark"));

            try {
                File file = new File(jsonObject.getString("signature_image"));
                builder.addFormDataPart("signature_image", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            } catch (Exception e) {
                e.printStackTrace();
            }


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstant.API_BASE_URL)
                    .build();


            WebServicesCheckOutAPI uploadService = retrofit.create(WebServicesCheckOutAPI.class);

            MultipartBody requestBody = builder.build();
            Call<ResponseBody> call = uploadService.uploadMultiFile(requestBody);
            call.enqueue(new Callback<ResponseBody>() {


                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        if (json.getString("status").equalsIgnoreCase("200")) {
                            new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.SUCCESS, jsonObject);
                        } else {
                            new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.FAIL, jsonObject);
                        }
                        Toast.makeText(context, json.getString("msg"), Toast.LENGTH_LONG).show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.FAIL, jsonObject);
                    Log.d(TAG, "Error " + t.getMessage());
                }
            });
        } catch (Exception e) {
            Toast.makeText(UploadIntentService.this, "Upload failed", Toast.LENGTH_SHORT).show();
            new SQLHelper(context).updateCheckOutData(SQLHelper.CHECK_OUT_TYPE, SQLHelper.FAIL, jsonObject);
            e.printStackTrace();
        }
    }

    private void uploadMailScanMultiFile(final JSONObject jsonObject) {

        MultipartBody.Builder builder = new MultipartBody.Builder();
        builder.setType(MultipartBody.FORM);

        try {
            String[] filePaths = jsonObject.getString("photo_list").split(",");
            builder.addFormDataPart("mailboxes", jsonObject.getString("mailboxid"));
            builder.addFormDataPart("remark", jsonObject.getString("remark"));
            builder.addFormDataPart("scan_unique", System.currentTimeMillis()+"");

            // Map is used to multipart the file using okhttp3.RequestBody
            // Multiple Images
            for (int i = 0; i < filePaths.length; i++) {
                File file = new File(filePaths[i]);
                builder.addFormDataPart("scan_images[]", file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file));
            }

            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(AppConstant.API_BASE_URL)
                    .build();


            WebServicesMailScanAPI uploadService = retrofit.create(WebServicesMailScanAPI.class);

            MultipartBody requestBody = builder.build();
            Call<ResponseBody> call = uploadService.uploadMultiFile(requestBody);
            call.enqueue(new Callback<ResponseBody>() {


                @Override
                public void onResponse(Call<ResponseBody> call, retrofit2.Response<ResponseBody> response) {
//                    new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.SUCCESS, jsonObject);
                    try {
                        JSONObject json = new JSONObject(response.body().string());
                        if (json.getString("status").equalsIgnoreCase("200")) {
                            new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.SUCCESS, jsonObject);
                        } else {
                            new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.FAIL, jsonObject);
                        }
                        Toast.makeText(context, json.getString("msg"), Toast.LENGTH_LONG).show();

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    Log.e(TAG, "Error " + t.getMessage());
                    Toast.makeText(UploadIntentService.this, "Upload failed", Toast.LENGTH_SHORT).show();
                    new SQLHelper(context).updateMailScanData(SQLHelper.MAIL_SCAN_TYPE, SQLHelper.FAIL, jsonObject);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}