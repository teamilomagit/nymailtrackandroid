package com.tiu.nymailtrack.dbHelper;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SQLHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "nyMail.db";
    private SQLiteDatabase database;

    private Context context;

    public static String CHECK_IN_TYPE = "CHECK_IN_TYPE";
    public static String CHECK_OUT_TYPE = "CHECK_OUT_TYPE";
    public static String MAIL_SCAN_TYPE = "MAIL_SCAN_TYPE";

    public static String IN_PROGRESS = "In Progress";
    public static String SUCCESS = "SUCCESS";
    public static String FAIL = "FAIL";
    public static String MAIL_ID_DOES_NOT_EXIST = "MAIL_ID_DOES_NOT_EXIST";
    public static String ALREADY_EXIST = "Tracking number already exist for package.";

    //Table Name
    public static final String TABLE_MAIL_STATUS = "TABLE_MAIL_STATUS";

    //Attributes
    public static final String MAIL_BOX = "MAIL_BOX";
    public static final String TRACK_NO = "TRACK_NO";
    public static final String DATA = "DATA";
    public static final String STATUS = "QUOTES";
    public static final String TYPE = "TYPE";
    public static final String ID = "ID";
    public static final String DATE = "DATE";
    public static final String CHECK_OUT_ACCOUNT_ID = "CHECK_OUT_ACCOUNT_ID";
    public static final String MAIL_SCAN_ID = "MAIL_SCAN_ID";

    public SQLHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        sqLiteDatabase.execSQL("create table " + TABLE_MAIL_STATUS + " ( " +
                ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                MAIL_BOX + " string , " +
                TRACK_NO + " string , " +
                DATA + " string ," +
                STATUS + " string, " +
                DATE + " string, " +
                CHECK_OUT_ACCOUNT_ID + " string, " +
                MAIL_SCAN_ID + " string, " +
                TYPE + " string);");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_MAIL_STATUS);
    }


    public void insertCheckInData(String type, JSONObject jsonObject) {
        database = this.getReadableDatabase();

        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(MAIL_BOX, jsonObject.getString("mailboxid"));
            contentValues.put(TRACK_NO, jsonObject.getString("tracking_number"));
            contentValues.put(MAIL_SCAN_ID, jsonObject.getString("scan_id"));
            contentValues.put(DATA, new Gson().toJson(jsonObject));
            contentValues.put(STATUS, IN_PROGRESS);
            contentValues.put(TYPE, type);
            contentValues.put(DATE, new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(new Date()));

            long record = database.insert(TABLE_MAIL_STATUS, null, contentValues);
            Log.e("Insert Data", record + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        database.close();
    }


    public void insertCheckOutData(String type, JSONObject jsonObject) {
        database = this.getReadableDatabase();

        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(MAIL_BOX, jsonObject.getString("mailboxid"));
            contentValues.put(CHECK_OUT_ACCOUNT_ID, jsonObject.getString("account_id"));
            contentValues.put(DATA, new Gson().toJson(jsonObject));
            contentValues.put(STATUS, IN_PROGRESS);
            contentValues.put(TYPE, type);
            contentValues.put(DATE, new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(new Date()));

            long record = database.insert(TABLE_MAIL_STATUS, null, contentValues);
            Log.e("Insert Data", record + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        database.close();
    }

    public void insertMailScanData(String type, JSONObject jsonObject) {
        database = this.getReadableDatabase();

        ContentValues contentValues = new ContentValues();
        try {
            contentValues.put(MAIL_BOX, jsonObject.getString("mailboxid"));
            contentValues.put(MAIL_SCAN_ID, jsonObject.getString("scan_id"));
            contentValues.put(DATA, new Gson().toJson(jsonObject));
            contentValues.put(STATUS, IN_PROGRESS);
            contentValues.put(TYPE, type);
            contentValues.put(DATE, new SimpleDateFormat("MM/dd/yyyy hh:mm a").format(new Date()));

            long record = database.insert(TABLE_MAIL_STATUS, null, contentValues);
            Log.e("Insert Data", record + "");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        database.close();
    }


    public void updateCheckInData(String type, String status, JSONObject jsonObject) {
        database = this.getReadableDatabase();

        try {


            String query = "SELECT * FROM " + TABLE_MAIL_STATUS + " WHERE " + TYPE + " = '" + type +
                    "' AND " + MAIL_SCAN_ID + " = '" + jsonObject.getString("scan_id") + "'";

            /*String query = "SELECT * FROM " + TABLE_MAIL_STATUS + " WHERE " + TYPE + " = '" + type +
                    "' AND " + MAIL_BOX + " = '" + jsonObject.getString("mailboxid") +
                    "' AND " + TRACK_NO + " = '" + jsonObject.getString("tracking_number") +
                    "' AND " + MAIL_SCAN_ID + " = '" + jsonObject.getString("scan_id") + "'";*/

            Cursor cursor = database.rawQuery(query, null);

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToNext();
                /* db.execSQL("UPDATE " + TABLE_QUOTES + " SET " + IS_QUOTE_OF_DAY + " = "
                    + 1 + " WHERE " + ID + " = '" + id + "'");*/
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MAIL_BOX, jsonObject.getString("mailboxid"));
                    contentValues.put(TRACK_NO, jsonObject.getString("tracking_number"));
                    contentValues.put(DATA, new Gson().toJson(jsonObject));
                    contentValues.put(STATUS, status);
                    contentValues.put(TYPE, type);

                    String trackNo = jsonObject.getString("tracking_number");

//                    long record = database.update(TABLE_MAIL_STATUS, contentValues, " TRACK_NO " + "=\"" + trackNo + "\"", null);
                    long record = database.update(TABLE_MAIL_STATUS, contentValues, " MAIL_SCAN_ID = " + jsonObject.getString("scan_id").trim(), null);
                    Log.e("Update Data", record + "");
                    context.sendBroadcast(new Intent("refresh"));
                } else {
                    insertCheckInData(type, jsonObject);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        database.close();
    }


    public void updateCheckOutData(String type, String status, JSONObject jsonObject) {
        database = this.getReadableDatabase();

        try {

            String query = "SELECT * FROM " + TABLE_MAIL_STATUS + " WHERE " + TYPE + " = '" + type +
                    "' AND " + MAIL_BOX + " = '" + jsonObject.getString("mailboxid") +
                    "' AND " + CHECK_OUT_ACCOUNT_ID + " = '" + jsonObject.getString("account_id") + "'";

            Cursor cursor = database.rawQuery(query, null);

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToNext();
                /* db.execSQL("UPDATE " + TABLE_QUOTES + " SET " + IS_QUOTE_OF_DAY + " = "
                    + 1 + " WHERE " + ID + " = '" + id + "'");*/
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MAIL_BOX, jsonObject.getString("mailboxid"));
                    contentValues.put(CHECK_OUT_ACCOUNT_ID, jsonObject.getString("account_id"));
                    contentValues.put(DATA, new Gson().toJson(jsonObject));
                    contentValues.put(STATUS, status);
                    contentValues.put(TYPE, type);

                    long record = database.update(TABLE_MAIL_STATUS, contentValues, " CHECK_OUT_ACCOUNT_ID = " + jsonObject.getString("account_id"), null);
                    Log.e("Update Data", record + "");
                    context.sendBroadcast(new Intent("refresh"));
                } else {
                    insertCheckOutData(type, jsonObject);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }


    public void updateMailScanData(String type, String status, JSONObject jsonObject) {

        database = this.getReadableDatabase();
        try {

            String query = "SELECT * FROM " + TABLE_MAIL_STATUS + " WHERE " + TYPE + " = '" + type +
                    "' AND " + MAIL_BOX + " = '" + jsonObject.getString("mailboxid") +
                    "' AND " + MAIL_SCAN_ID + " = '" + jsonObject.getString("scan_id") + "'";

            Cursor cursor = database.rawQuery(query, null);

            if (cursor != null) {
                if (cursor.getCount() > 0) {
                    cursor.moveToNext();
                /* db.execSQL("UPDATE " + TABLE_QUOTES + " SET " + IS_QUOTE_OF_DAY + " = "
                    + 1 + " WHERE " + ID + " = '" + id + "'");*/
                    ContentValues contentValues = new ContentValues();
                    contentValues.put(MAIL_BOX, jsonObject.getString("mailboxid"));
                    contentValues.put(MAIL_SCAN_ID, jsonObject.getString("scan_id"));
                    contentValues.put(DATA, new Gson().toJson(jsonObject));
                    contentValues.put(STATUS, status);
                    contentValues.put(TYPE, type);

                    long record = database.update(TABLE_MAIL_STATUS, contentValues, " MAIL_SCAN_ID = " + jsonObject.getString("scan_id").trim(), null);
                    Log.e("Update Data", record + "");
                    context.sendBroadcast(new Intent("refresh"));
                } else {
                    insertMailScanData(type, jsonObject);
                }
                cursor.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        database.close();
    }


    public ArrayList<JSONObject> fetchMailStatusWithType(String type) {
        database = this.getReadableDatabase();

//        Cursor cursor = database.rawQuery("SELECT * FROM " + TABLE_QUOTES + " WHERE " + ID + " IN " +
//                "(SELECT "+ CONTENT_ID + " FROM " + TABLE_FAVOURITES
//                + ")", null);

        String query = "SELECT * FROM " + TABLE_MAIL_STATUS + " WHERE " + TYPE + " = '" + type + "'" + " order by ID desc";

        Cursor cursor = database.rawQuery(query, null);

        ArrayList<JSONObject> list = new ArrayList<>();

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {

                    JSONObject jsonObject = new Gson().fromJson(cursor.getString(3), JSONObject.class);
                    try {
                        jsonObject.put("status", cursor.getString(4));
                        jsonObject.put("date", cursor.getString(5));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    list.add(jsonObject);
                } while (cursor.moveToNext());
            }
        }
        cursor.close();
        database.close();

        return list;
    }
}
