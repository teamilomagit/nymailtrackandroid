package com.tiu.nymailtrack.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.DashboardActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class CustomCamera extends AppCompatActivity {

    private Context context;
    private SurfaceView mSurfaceView;
    private SurfaceHolder mSurfaceHolder;
    private TextView txtClickCount;

    private Camera mCamera;
    private boolean mPreviewRunning = false;

    ArrayList<String> photoPath = new ArrayList<>();

    private int hasItem;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        context = this;

        setContentView(R.layout.layout_custom_camera);

        //has item extra
        hasItem = getIntent().getIntExtra("has_items", 0);

        txtClickCount = findViewById(R.id.txt_click_count);

        mSurfaceView = findViewById(R.id.surface_camera);

        mSurfaceHolder = mSurfaceView.getHolder();

        mSurfaceHolder.addCallback(new SurfaceHolder.Callback() {
            @Override
            public void surfaceCreated(SurfaceHolder holder) {
                mCamera = Camera.open();
            }

            @Override
            public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
                if (mPreviewRunning) {

                    mCamera.stopPreview();

                }

//                Camera.Parameters p = mCamera.getParameters();
//                p.setPreviewSize(width, height);
//                mCamera.setParameters(p);

                mCamera.setDisplayOrientation(90);

                Camera.Parameters parameters = mCamera.getParameters();
                parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
                mCamera.setParameters(parameters);


                try {
                    mCamera.setPreviewDisplay(holder);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                mCamera.startPreview();

                mPreviewRunning = true;
            }

            @Override
            public void surfaceDestroyed(SurfaceHolder holder) {
                mCamera.stopPreview();
                mPreviewRunning = false;
                mCamera.release();
            }
        });

//        mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void takeSnap(View view) {
        mCamera.takePicture(null, null, new Camera.PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                if (photoPath.size() < 10 && hasItem != 10) {
                    camera.stopPreview();
                    camera.startPreview();
                    Bitmap bitmap = BitmapFactory.decodeByteArray(data, 0, data.length);
                    storeImage(getResizedBitmap(bitmap, 800));
//                Toast.makeText(context, "Image saved", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Maximum 10 images allowed", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    public void finishWithResults(View view) {
        if (photoPath.size() > 0) {
            Intent data = new Intent();
            data.putStringArrayListExtra("images", photoPath);
            setResult(Activity.RESULT_OK, data);
            finish();
        } else {
            Toast.makeText(context, "Please select at least 1 photo", Toast.LENGTH_SHORT).show();
        }
    }

    public void finishActivity(View view) {
        if (hasItem > 0) {
            finish();
        } else {
            startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }
    }

    private void storeImage(Bitmap image) {
        String path = Environment.getExternalStorageDirectory().toString() + "/Pictures/" + System.currentTimeMillis() + ".png";
        File pictureFile = new File(path);
        if (pictureFile == null) {
            Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show();
            return;
        }
        try {
            FileOutputStream fos = new FileOutputStream(pictureFile);
            image.compress(Bitmap.CompressFormat.PNG, 0, fos);
            fos.close();
            photoPath.add(path);
            txtClickCount.setText(String.valueOf(photoPath.size()));
            ++hasItem;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (hasItem > 0) {
            finish();
        } else {
            startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
        }
    }
}
