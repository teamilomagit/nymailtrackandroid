package com.tiu.nymailtrack.util;

import android.text.TextUtils;

public class Utility {

    public final static boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public final static boolean isValidMobile(String phone) {
        return android.util.Patterns.PHONE.matcher(phone).matches();
    }


    public static String getRandomId() {

        int random = (int) (Math.random() * 500 + 1);

        return System.currentTimeMillis() + "" + random;

    }

}
