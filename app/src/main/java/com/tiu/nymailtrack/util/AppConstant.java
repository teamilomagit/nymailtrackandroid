package com.tiu.nymailtrack.util;

import com.tiu.nymailtrack.model.CheckInModel;

public class AppConstant {

//        public static String API_BASE_URL = "http://dev.fifthavenueaddress.com/";
    public static String API_BASE_URL = "https://fifthavenueaddress.com/";

    public static String API_LOGIN = API_BASE_URL + "apps/login";
    public static String API_FETCH_COURIER = API_BASE_URL + "checkins/courierList";
    public static String API_CHCKINS_PACKAGE = API_BASE_URL + "checkins/package";
    public static String API_CHCKOUT_LIST = API_BASE_URL + "checkouts/customer";
    public static String API_PARCEL_LIST = API_BASE_URL + "Checkouts/parcelDetails";
    public static String API_CHECKOUT_SAVE = API_BASE_URL + "Checkouts/checkoutSave";
    public static String API_CHECKINS_MAILSCAN = API_BASE_URL + "custmailboxes/mailscanner";
    public static String API_MAILCHECKER = API_BASE_URL + "custmailboxes/mailChecker";
    public static String API_PARCELHISTORY = API_BASE_URL + "Checkouts/appHistoryParcel";
    public static String API_APPHISTORY = API_BASE_URL + "Checkouts/appHistory";

    public static CheckInModel constantCheckInModel = null;

    //FLAGS
    public static boolean SHOULD_RELOAD = false;

    //Constant string
    //public static String ALREADY_EXIST_MSG = "Mail Box id and tracking number you enter are already exist for package.";
    public static String ALREADY_EXIST_MSG = "Tracking number already exist for package.";
    public static String MAIL_BOX_DOES_NOT_EXIST = "Mail Box id not match with any customer. Please try again!";

}
