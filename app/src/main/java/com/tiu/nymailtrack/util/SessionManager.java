package com.tiu.nymailtrack.util;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.tiu.nymailtrack.activity.LoginActivity;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.model.LoginModel;

/**
 * Created by Mohsin on 1/22/2019.
 */


public class SessionManager {

    SharedPreferences pref;
    SharedPreferences.Editor editor;
    Context context;

    // Shared pref mode
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "AndroidHivePref";
    private static final String IS_LOGIN = "IsLoggedIn";
    public static final String KEY_USER = "user";

    public SessionManager(Context context) {

        this.context = context;
        pref = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();

    }

    public void createLoginSession(LoginModel loginModel) {
        Gson gson = new Gson();
        String jsonString = gson.toJson(loginModel);
        editor.putBoolean(IS_LOGIN, true);
        editor.putString(KEY_USER, jsonString);
        editor.commit();
    }

    public LoginModel getLoginModel() {
        LoginModel loginModel = null;
        Gson gson = new Gson();
        String jsonString = pref.getString(KEY_USER, "");
        loginModel = gson.fromJson(jsonString, LoginModel.class);
        return loginModel;
    }


    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }

    /**
     * Clear session details
     */
    public void logoutUser() {
        editor.putBoolean(IS_LOGIN, false);
        editor.putString(KEY_USER, null);
        editor.commit();
        clearPreferences();
        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(i);
    }

    public void clear() {
        editor.putString(KEY_USER, null);
        editor.commit();
    }

    private void clearPreferences() {
        //new AppPreferences(context).clearPackageList();
    }

}
