package com.tiu.nymailtrack.util;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

public class AppPreferences {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    private final static String PREF_NAME = "NyMailTrackAndroid";
    public static String PREF_USERNAME = "username";
    public static String PREF_PASSWORD = "password";
    int PRIVATE_MODE = 0;

    public AppPreferences(Context context) {
        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        this.context = context;
        this.editor = sharedPreferences.edit();
    }

    public String getEmail() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return prefs.getString(PREF_USERNAME, null);
    }

    public void setEmail(String value) {
        editor.putString(PREF_USERNAME, value);
        editor.commit();
    }

    public String getPassword() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return prefs.getString(PREF_PASSWORD, null);
    }

    public void setPassword(String value) {
        editor.putString(PREF_PASSWORD, value);
        editor.commit();
    }

    public void setCourierList(String value) {
        editor.putString("courier_list", value);
        editor.commit();
    }

    public String getCourierList() {
        SharedPreferences prefs = context.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        return prefs.getString("courier_list", null);
    }
}
