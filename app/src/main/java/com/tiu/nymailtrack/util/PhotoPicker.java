package com.tiu.nymailtrack.util;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.widget.ImageView;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;


public class PhotoPicker {

    Context context;
    Bitmap bitmap;
    private ImageView imgSelectPic;
    Uri uri, filePath;

    static String[] PERMISSIONS = {Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA};
    public static final int PERMISSION_ALL = 1;
    public static int REQUEST_CAMERA = 90, SELECT_FILE = 91;

    public PhotoPicker(Context context) {
        this.context = context;
    }

    public  void selectImage(ImageView img) {
        imgSelectPic = img;
        final CharSequence[] items = {"Take Photo", "Choose From Gallery", "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Add Image");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Take Photo")) {
                    cameraIntent();
                } else if (items[item].equals("Choose From Gallery")) {
                    galleryIntent();
                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    public void cameraIntent() {
        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From your Camera");
        uri = context.getContentResolver().insert(
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, uri);
        ((Activity) context).startActivityForResult(intent, REQUEST_CAMERA);
    }

    public void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        ((Activity) context).startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }


    @SuppressWarnings("deprecation")
    public Uri onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(context.getContentResolver(), data.getData());
                imgSelectPic.setImageBitmap(bm);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return filePath = data.getData();
        }
        return null;
    }

    public Uri onCaptureImageResult() {
        Bitmap photo = null;
        try {
            photo = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
            if (imgSelectPic!= null) {
                imgSelectPic.setImageBitmap(getResizedBitmap(photo, 800));
            }
//            SaveImage(getResizedBitmap(photo, 800));
            return filePath = getImageUri(getResizedBitmap(photo, 800));
        } catch (Exception e) {
            e.printStackTrace();
        }
        // CALL THIS METHOD TO GET THE URI FROM THE BITMAP
        return null;
    }




    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float) width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }

        bitmap = Bitmap.createScaledBitmap(image, width, height, true);
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void SaveImage(Bitmap finalBitmap) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/NyMail");

        if (!myDir.exists()) {
            myDir.mkdirs();
            myDir = new File(root + "/NyMail/Images");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
        } else {
            myDir = new File(root + "/NyMail/Images");
            if (!myDir.exists()) {
                myDir.mkdirs();
            }
        }
        Random generator = new Random();
        int n = 10000;
        n = generator.nextInt(n);
        String fname = "imgNyMail" + n + ".jpg";
        File file = new File(myDir, fname);
        if (file.exists()) file.delete();
        try {
            FileOutputStream out = new FileOutputStream(file);
            finalBitmap.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            filePath = getImageUri(finalBitmap);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Uri getImageUri(Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 70, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static String getPath(Uri uri, final Context context) {
        Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        String document_id = cursor.getString(0);
        document_id = document_id.substring(document_id.lastIndexOf(":") + 1);
        cursor.close();

        cursor = context.getContentResolver().query(
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                null, MediaStore.Images.Media._ID + " = ? ", new String[]{document_id}, null);
        cursor.moveToFirst();
        String path = cursor.getString(cursor.getColumnIndex(MediaStore.Images.Media.DATA));
        cursor.close();
        return path;
    }

    public boolean isPermissionGranted(Context context){

        if (!hasPermission(context,PERMISSIONS)){

            if(ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,Manifest.permission.CAMERA)){
                ActivityCompat.requestPermissions((Activity) context,PERMISSIONS,PERMISSION_ALL);
                return false;
            }else if (ActivityCompat.shouldShowRequestPermissionRationale((Activity) context,Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                ActivityCompat.requestPermissions((Activity) context,PERMISSIONS,PERMISSION_ALL);
                return false;
            }else{
                ActivityCompat.requestPermissions((Activity) context, PERMISSIONS, PERMISSION_ALL);
                return false;
            }
        }else
           return true;
    }

    private boolean hasPermission(Context context,String... permissions){

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

}
