package com.tiu.nymailtrack.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.SparseArray;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.vision.barcode.Barcode;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.BaseActivity;
import com.tiu.nymailtrack.activity.DashboardActivity;
import com.tiu.nymailtrack.adapter.SelectionAdapter;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;
import com.tiu.nymailtrack.util.PhotoPicker;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import info.androidhive.barcode.BarcodeReader;

public class ScannerDialog extends Dialog implements BarcodeReader.BarcodeReaderListener {

    BarcodeReader barcodeReader;
    private Context context;
    TextView txtSkip;
    private PhotoPicker photoPicker;
    String barcodeValue;


    public ScannerDialog(Context context,PhotoPicker photoPicker) {
        super(context);
        this.context = context;
        this.photoPicker = photoPicker;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_scanner);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        init();
    }

    private void init() {

        barcodeReader = (BarcodeReader) ((AppCompatActivity)context).getSupportFragmentManager().findFragmentById(R.id.barcode_scanner);
        barcodeReader.setListener(this);
        txtSkip = findViewById(R.id.txt_skip);

        setOnClick();
        setToolBarWithTitleAndBack();
    }

    protected void setToolBarWithTitleAndBack() {
        TextView txtTitle = findViewById(R.id.txt_toolbar_title);
        txtTitle.setText("Scan Tracking Number");


        ImageView imgBack = findViewById(R.id.img_back);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
            }
        });
    }

    private void setOnClick() {

        txtSkip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                barcodeReader.getFragmentManager().beginTransaction().remove(barcodeReader).commit();
                dismiss();
            }
        });
    }

    @Override
    public void onScanned(Barcode barcode) {

        // playing barcode reader beep sound
        barcodeReader.playBeep();
        barcodeValue = barcode.displayValue;

//        listener.getBarcodeId(barcodeValue);
        photoPicker.cameraIntent();
        barcodeReader.getFragmentManager().beginTransaction().remove(barcodeReader).commit();

        context.sendBroadcast(new Intent("tracking_number").putExtra("barcode", barcodeValue));
        dismiss();
    }

    @Override
    public void onScannedMultiple(List<Barcode> barcodes) {

    }

    @Override
    public void onBitmapScanned(SparseArray<Barcode> sparseArray) {

    }

    @Override
    public void onScanError(String errorMessage) {
        Toast.makeText(context, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCameraPermissionDenied() {
        dismiss();
    }


    @Override
    public void onBackPressed() {
        context.startActivity(new Intent(context, DashboardActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP));
    }



}
