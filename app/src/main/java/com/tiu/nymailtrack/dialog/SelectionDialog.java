package com.tiu.nymailtrack.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Button;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.adapter.SelectionAdapter;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.util.APIManager;
import com.tiu.nymailtrack.util.AppConstant;


import org.json.JSONObject;

import java.util.ArrayList;

public class SelectionDialog extends Dialog {

    private Context context;
    private ArrayList<DashBoardModel> list;
    private SelectionDialogInterface listener;
    private SelectionAdapter mAdapter;
    private String url, paramId;

    public interface SelectionDialogInterface{
        void getId(String ID, String name);
    }

    public SelectionDialog(Context context , ArrayList<DashBoardModel> list, SelectionDialogInterface listener) {
        super(context);
        this.context = context;
        this.url = url;
        this.paramId = paramId;
        this.listener =listener;
        this.list = list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_selection);

        init();
    }

    private void init() {

        if (list.size() == 0) {
            getCourierData();
        }
        setRecyclerView();
    }



    private void setRecyclerView(){
        mAdapter = new SelectionAdapter(context, list, new SelectionAdapter.SelectionAdapterInterface() {
            @Override
            public void getItemPosition(int position) {
                if (listener != null) {
                    listener.getId(list.get(position).getId(), list.get(position).getName());
                    dismiss();
                }
            }
        });
        RecyclerView mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context));
        mRecyclerView.setAdapter(mAdapter);
    }

    /**
     * API CALLING
     **/
    private void getCourierData() {
        JSONObject jsonObject = new JSONObject();
        new APIManager().PostJsonArrayAPI(AppConstant.API_FETCH_COURIER, jsonObject, DashBoardModel.class, context, new APIManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObject, Object extraInfo, String message) {
                list = (ArrayList<DashBoardModel>) resultObject;
                mAdapter.updateAdapter(list);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(context, error, Toast.LENGTH_SHORT).show();
                dismiss();
            }
        });


    }

}
