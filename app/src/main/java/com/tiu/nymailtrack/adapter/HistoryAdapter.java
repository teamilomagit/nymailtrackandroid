package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.CustomerDetailActivity;
import com.tiu.nymailtrack.activity.ParcelHistoryActivity;
import com.tiu.nymailtrack.model.CheckOutModel;

import java.io.Serializable;
import java.util.ArrayList;

public class HistoryAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<CheckOutModel> list;

    private boolean isLoadingAdded = false;
    private static final int ITEM = 0;
    private static final int LOADING = 1;

    public HistoryAdapter(Context context, ArrayList<CheckOutModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());


        switch (viewType) {
            case ITEM:
                View v1 = inflater.inflate(R.layout.list_item_checkout, viewGroup, false);
                viewHolder = new MyViewHolder(v1);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.item_progress, viewGroup, false);
                viewHolder = new LoadingVH(v2);
                break;
        }
        return viewHolder;
//        return new HistoryAdapter.MyViewHolder(LayoutInflater.from(viewGroup.getRootView().getContext()).inflate(R.layout.list_item_checkout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, final int position) {

        switch (getItemViewType(position)) {

            case ITEM:
                final CheckOutModel model = list.get(position);
                final MyViewHolder myViewHolder = (MyViewHolder) holder;

                myViewHolder.imgDelete.setVisibility(View.GONE);
//                myViewHolder.txtCustomerName.setText(model.getFirst_name() + " " + model.getLast_name());
                myViewHolder.txtCustomerName.setText(model.getMailboxes());
                myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        context.startActivity(new Intent(context, ParcelHistoryActivity.class).putExtra("model", (Serializable) model));
                    }
                });
                break;

            case LOADING:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateAdapter(ArrayList<CheckOutModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtCustomerName;
        ImageView imgDelete;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtCustomerName = itemView.findViewById(R.id.txt_customer_name);
            imgDelete = itemView.findViewById(R.id.img_delete);
        }
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {

        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public int getItemViewType(int position) {
        return (position == list.size() - 1 && isLoadingAdded) ? LOADING : ITEM;
    }

     /*
   Helpers
   _________________________________________________________________________________________________
    */

    public void addAll(ArrayList<CheckOutModel> pmList) {
        for (CheckOutModel pd : pmList) {
            add(pd);
        }
    }

    public void add(CheckOutModel gymAdminModel) {
        list.add(gymAdminModel);
        notifyItemInserted(list.size() - 1);
    }

    public void remove(CheckOutModel gymAdminModel) {
        int position = list.indexOf(gymAdminModel);
        if (position > -1) {
            list.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        list.clear();
        notifyDataSetChanged();
    }

    /**
     * ADD AND REMOVE LOADING FOOTER
     **/
    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new CheckOutModel());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        if (list.size() > 0) {
            int position = list.size() - 1;
            CheckOutModel item = getItem(position);

            if (item != null) {
                list.remove(position);
                notifyItemRemoved(position);
            }
        }
    }

    public CheckOutModel getItem(int position) {
        return list.get(position);
    }
}
