package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.jsibbold.zoomage.ZoomageView;
import com.tiu.nymailtrack.R;

import java.util.ArrayList;

public class ImagePagerAdapter extends PagerAdapter {

    private LayoutInflater inflater;
    private Context context;
    private ArrayList<String> list;

    public ImagePagerAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return PagerAdapter.POSITION_NONE;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View myImageLayout = inflater.inflate(R.layout.item_slide_fullimage, view, false);
        ZoomageView myImage = myImageLayout
                .findViewById(R.id.image);


        Glide.with(context).load(list.get(position).replace(" ", "")).thumbnail(0.4f).into(myImage);

        view.addView(myImageLayout, 0);
        return myImageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }


}
