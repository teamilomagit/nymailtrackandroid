package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.FullscreenImageActivity;

import java.util.ArrayList;

public class ImageAdapter extends RecyclerView.Adapter<ImageAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<String> list;

    public ImageAdapter(Context context, ArrayList<String> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public ImageAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(context).inflate(R.layout.single_image_list, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ImageAdapter.MyViewHolder myViewHolder, final int position) {

        Glide.with(context).load(list.get(position).replace(" ","")).thumbnail(0.5f).into(myViewHolder.imgBarcode);

        if (list.get(position).contains("http")) {
            myViewHolder.imgRemove.setVisibility(View.GONE);
        } else {
            myViewHolder.imgRemove.setVisibility(View.VISIBLE);
        }

        // To Delete Perticular Image
        myViewHolder.imgRemove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                list.remove(position);
                updateAdapter(list);
            }
        });

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, FullscreenImageActivity.class)
                        .putExtra("position", position)
                        .putExtra("image_list", list));
            }
        });

    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }


    public void updateAdapter(ArrayList<String> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgBarcode, imgRemove;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            imgBarcode = itemView.findViewById(R.id.img_barcode_list);
            imgRemove = itemView.findViewById(R.id.img_remove);
        }
    }
}
