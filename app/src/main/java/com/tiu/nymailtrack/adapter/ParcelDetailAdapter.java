package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.model.CheckOutModel;
import com.tiu.nymailtrack.model.ParcelDetailModel;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class ParcelDetailAdapter extends RecyclerView.Adapter<ParcelDetailAdapter.MyViewHolder> {
    Context context;
    private ArrayList<ParcelDetailModel> list;
    private int count;

    public ParcelDetailAdapter(Context context, ArrayList<ParcelDetailModel> list) {
        this.context = context;
        this.list = list;
    }


    @NonNull
    @Override
    public ParcelDetailAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_parcel_detail, viewGroup, false);
        return new ParcelDetailAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ParcelDetailAdapter.MyViewHolder myViewHolder, final int position) {
        final ParcelDetailModel model = list.get(position);
        myViewHolder.txtTrackingNumber.setText(model.getTracking_number());
        myViewHolder.txtOriginalCharge.setText("$" + String.format("%.2f", model.getPackageTotalDue()) + " " + getAmountType(model.getAmount_type()));
        myViewHolder.txtTotalDue.setText("$" + String.format("%.2f", model.getPackageTotalDue()));

        try {
            myViewHolder.txtDate.setText(new SimpleDateFormat("MM/dd/yyyy")
                    .format(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(model.getReceivedate())));
        } catch (Exception e) {
            e.printStackTrace();
        }


        myViewHolder.chkParcel.setChecked(model.isChecked());


        myViewHolder.chkParcel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                list.get(position).setChecked(myViewHolder.chkParcel.isChecked());
            }
        });

    }

    public Object[] getSelectedItemTotal() {

        Object[] arr = new Object[3];

        double total = 0;
        int count = 0;
        String selectedIds = "";

        for (ParcelDetailModel model : list) {
            if (model.isChecked()) {
                total = total + model.getPackageTotalDue();
                count++;
                selectedIds = selectedIds + model.getId() + ",";
            }
        }
        arr[0] = total;
        arr[1] = count;
        arr[2] = selectedIds;

        return arr;
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateAdapter(ArrayList<ParcelDetailModel> list) {
        this.list = list;
        count = list.size();
        notifyDataSetChanged();
    }

    private String getAmountType(String val) {
        if (val.equalsIgnoreCase("1")) {
            return "First 3 days";
        } else if (val.equalsIgnoreCase("2")) {
            return "Flat";
        } else if (val.equalsIgnoreCase("3")) {
            return "Per day";
        }

        return "";
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTrackingNumber, txtTotalDue, txtOriginalCharge, txtDate;
        AppCompatCheckBox chkParcel;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTrackingNumber = itemView.findViewById(R.id.txt_tracking_number);
            txtTotalDue = itemView.findViewById(R.id.txt_total_due);
            txtOriginalCharge = itemView.findViewById(R.id.txt_original_charge);
            txtDate = itemView.findViewById(R.id.txt_date);
            chkParcel = itemView.findViewById(R.id.chk_parcel);
        }
    }
}
