package com.tiu.nymailtrack.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.LoginActivity;

import java.util.ArrayList;

public class MailCheckerAdapter extends RecyclerView.Adapter<MailCheckerAdapter.MyViewHolder> {
    Context context;
    ArrayList<String> list;
    Boolean isDelete = false;
    MailCheckerAdapterInterface listener;

    public MailCheckerAdapter(Context context, ArrayList<String> list, MailCheckerAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public interface MailCheckerAdapterInterface
    {
        void getDeleteItemPosition(int position);
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new MailCheckerAdapter.MyViewHolder(LayoutInflater.from(viewGroup.getRootView().getContext()).inflate(R.layout.list_item_checkout, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        myViewHolder.imgArrow.setVisibility(View.GONE);
        myViewHolder.txtTrackingMail.setText(list.get(position));
        myViewHolder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog(position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateAdapter(ArrayList<String> list){
        this.list = list;
        notifyDataSetChanged();
    }

    private void alertDialog(final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure, you want to remove this item from list?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                listener.getDeleteItemPosition(position);
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTrackingMail;
        ImageView imgArrow, imgDelete;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTrackingMail = itemView.findViewById(R.id.txt_customer_name);
            imgArrow = itemView.findViewById(R.id.img_arrow);
            imgDelete = itemView.findViewById(R.id.img_delete);
        }
    }
}
