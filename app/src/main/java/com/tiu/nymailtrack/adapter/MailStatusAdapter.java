package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.CheckInActivity;
import com.tiu.nymailtrack.activity.MailScanActivity;
import com.tiu.nymailtrack.dbHelper.SQLHelper;
import com.tiu.nymailtrack.model.MailboxModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class MailStatusAdapter extends RecyclerView.Adapter<MailStatusAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<JSONObject> list;
    private MailStatusAdapterInterface listener;

    public interface MailStatusAdapterInterface {
        void retryUploadForObject(JSONObject jsonObject);
    }

    public void updateAdapter(ArrayList<JSONObject> list, MailStatusAdapterInterface listener) {
        this.list = list;
        this.listener = listener;
        notifyDataSetChanged();
    }

    public MailStatusAdapter(Context context, ArrayList<JSONObject> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MailStatusAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_mail_box, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final MailStatusAdapter.MyViewHolder holder, int position) {
        final JSONObject jsonObject = list.get(position);

        try {

            final String type = jsonObject.getString("type");

            holder.txtDate.setText(jsonObject.getString("date"));
            holder.txtTrackingNumber.setVisibility(View.GONE);

            if (type.equalsIgnoreCase(SQLHelper.CHECK_IN_TYPE)) {
                holder.txtTrackingNumber.setVisibility(View.VISIBLE);
                holder.txtTrackingNumber.setText(jsonObject.getString("tracking_number"));
            }

            if (jsonObject.getString("status").equalsIgnoreCase(SQLHelper.IN_PROGRESS)) {

                //show retry button if duration is more than 1 mins, in case we donot get response of failure.
                if (isDurationLarge(jsonObject.getString("date"))) {
                    holder.txtStatus.setVisibility(View.GONE);
                    holder.cardStatus.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorButton));
                    holder.txtRetry.setVisibility(View.VISIBLE);
                } else {
                    holder.txtStatus.setText(context.getString(R.string.in_progress));
                    holder.txtStatus.setVisibility(View.VISIBLE);
                    holder.cardStatus.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorButton));
                    holder.txtRetry.setVisibility(View.GONE);
                }
            } else if (jsonObject.getString("status").equalsIgnoreCase(SQLHelper.SUCCESS)) {
                holder.txtStatus.setVisibility(View.GONE);
                holder.cardStatus.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
                holder.txtRetry.setVisibility(View.GONE);
            } else if (jsonObject.getString("status").equalsIgnoreCase(SQLHelper.ALREADY_EXIST)) {
                holder.txtStatus.setText(context.getString(R.string.alread_exist));
                holder.txtStatus.setVisibility(View.GONE);
                holder.cardStatus.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorGreen));
                holder.txtRetry.setVisibility(View.GONE);
            }else if (jsonObject.getString("status").equalsIgnoreCase(SQLHelper.MAIL_ID_DOES_NOT_EXIST)) {
                holder.txtStatus.setText(context.getString(R.string.mail_id_does_not_exist));
                holder.txtStatus.setVisibility(View.VISIBLE);
                holder.cardStatus.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorButton));
                holder.txtRetry.setVisibility(View.VISIBLE);
            } else {
                holder.txtStatus.setVisibility(View.GONE);
                holder.cardStatus.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorButton));
                holder.txtRetry.setVisibility(View.VISIBLE);
            }

            holder.txtRetry.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    holder.txtRetry.setVisibility(View.INVISIBLE);

                    if (type.equalsIgnoreCase(SQLHelper.MAIL_SCAN_TYPE)) {
                        context.startActivity(new Intent(context, MailScanActivity.class).putExtra("json", jsonObject.toString()));
                    } else if (type.equalsIgnoreCase(SQLHelper.CHECK_IN_TYPE)) {
                        context.startActivity(new Intent(context, CheckInActivity.class).putExtra("json", jsonObject.toString()));
                    } else {
                        holder.txtStatus.setVisibility(View.VISIBLE);
                        listener.retryUploadForObject(jsonObject);
                    }
                }
            });

            holder.txtMailId.setText(jsonObject.getString("mailboxid"));


        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private boolean isDurationLarge(String insertDate) {

        Date date1 = null;

        try {
            date1 = new SimpleDateFormat("MM/dd/yyyy hh:mm a").parse(insertDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        long diff = new Date().getTime() - date1.getTime();
        long seconds = diff / 1000;
        long minutes = seconds / 60;

        if (minutes >= 1) {
            return true;
        } else {
            return false;
        }

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        CardView cardStatus;
        TextView txtMailId, txtDate, txtStatus, txtRetry, txtTrackingNumber;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            cardStatus = itemView.findViewById(R.id.card_status);
            txtMailId = itemView.findViewById(R.id.txt_mail_box);
            txtDate = itemView.findViewById(R.id.txt_date);
            txtStatus = itemView.findViewById(R.id.txt_status);
            txtRetry = itemView.findViewById(R.id.txt_retry);
            txtTrackingNumber = itemView.findViewById(R.id.txt_tracking_number);
        }
    }
}
