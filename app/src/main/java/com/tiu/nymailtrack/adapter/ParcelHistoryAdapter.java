package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.ParcelDetailActivity;
import com.tiu.nymailtrack.model.CheckOutModel;
import com.tiu.nymailtrack.model.ParcelDetailModel;

import java.util.ArrayList;

public class ParcelHistoryAdapter extends RecyclerView.Adapter<ParcelHistoryAdapter.MyViewHolder> {
    Context context;
    ArrayList<ParcelDetailModel> list;

    public ParcelHistoryAdapter(Context context, ArrayList<ParcelDetailModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new ParcelHistoryAdapter.MyViewHolder(LayoutInflater.from(viewGroup.getRootView().getContext()).inflate(R.layout.list_item_parcel_history, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        final ParcelDetailModel model = list.get(position);

        if (!model.getTracking_number().equalsIgnoreCase("")) {
            myViewHolder.txtTrackingNumber.setText(model.getTracking_number());
        } else {
            myViewHolder.txtTrackingNumber.setText("Tracking No. Not Available");
        }

        myViewHolder.txtStatus.setText(model.getStatus());
        myViewHolder.txtStatus.setVisibility(View.INVISIBLE);

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(new Intent(context, ParcelDetailActivity.class)
                        .putExtra("parcel_model", new Gson().toJson(model)));
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public void updateAdapter(ArrayList<ParcelDetailModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtTrackingNumber, txtStatus;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtTrackingNumber = itemView.findViewById(R.id.txt_tracking_number);
            txtStatus = itemView.findViewById(R.id.txt_status);
        }
    }
}
