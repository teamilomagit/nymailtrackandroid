package com.tiu.nymailtrack.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.activity.CheckInActivity;
import com.tiu.nymailtrack.activity.CheckOutActivity;
import com.tiu.nymailtrack.activity.HistoryActivity;
import com.tiu.nymailtrack.activity.LoginActivity;
import com.tiu.nymailtrack.activity.MailCheckActivity;
import com.tiu.nymailtrack.activity.MailScanActivity;
import com.tiu.nymailtrack.activity.MailStatusActivity;
import com.tiu.nymailtrack.model.DashBoardModel;
import com.tiu.nymailtrack.util.SessionManager;

import java.util.ArrayList;

public class DashboardAdapter extends RecyclerView.Adapter<DashboardAdapter.MyViewHolder> {
    Context context;
    ArrayList<DashBoardModel> list;

    public DashboardAdapter(Context context, ArrayList<DashBoardModel> list) {
        this.context = context;
        this.list = list;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        return new DashboardAdapter.MyViewHolder(LayoutInflater.from(parent.getRootView().getContext()).inflate(R.layout.list_item_dashboard, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder myViewHolder, final int position) {
        DashBoardModel model = list.get(position);
        myViewHolder.imgMenu.setImageResource(model.getMenu_image());
        myViewHolder.txtMenuName.setText(model.getMenu_name());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                switch (position) {
                    case 0:
                        context.startActivity(new Intent(context, CheckInActivity.class));
                        break;
                    case 1:
                        context.startActivity(new Intent(context, CheckOutActivity.class).putExtra("flag", true));
                        break;
                    case 2:
                        context.startActivity(new Intent(context, MailScanActivity.class));
                        break;
                    case 99:
                        context.startActivity(new Intent(context, MailCheckActivity.class));
                        break;
                    case 3:
                        context.startActivity(new Intent(context, HistoryActivity.class));
                        break;
                    case 4:
                        context.startActivity(new Intent(context, MailStatusActivity.class));
                        break;
                    case 5:
                        alertDialog();
                        break;
                }
            }
        });

    }

    private void alertDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle("Logout");
        builder.setMessage("Are you sure, you want to logout?");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new SessionManager(context).logoutUser();
                context.startActivity(new Intent(context, LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
            }
        });

        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView imgMenu;
        TextView txtMenuName;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtMenuName = itemView.findViewById(R.id.txt_menu_name);
            imgMenu = itemView.findViewById(R.id.img_menu);
        }
    }
}
