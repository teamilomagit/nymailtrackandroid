package com.tiu.nymailtrack.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tiu.nymailtrack.R;
import com.tiu.nymailtrack.model.DashBoardModel;

import java.util.ArrayList;

public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.MyViewHolder> {

    Context context;
    ArrayList<DashBoardModel> list;
    SelectionAdapterInterface listener;

    public SelectionAdapter(Context context, ArrayList<DashBoardModel> list, SelectionAdapterInterface listener) {
        this.context = context;
        this.list = list;
        this.listener = listener;
    }

    public interface SelectionAdapterInterface {
        void getItemPosition(int position);
    }

    public void updateAdapter(ArrayList<DashBoardModel> list) {
        this.list = list;
        notifyDataSetChanged();
    }



    @NonNull
    @Override
    public SelectionAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_selection, viewGroup, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SelectionAdapter.MyViewHolder myViewHolder,final int position) {

        final DashBoardModel dashBoardModel = list.get(position);

        myViewHolder.txtName.setText(dashBoardModel.getName());

        myViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.getItemPosition(position);
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        TextView txtName;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            txtName =itemView.findViewById(R.id.txt_name);
        }
    }
}
